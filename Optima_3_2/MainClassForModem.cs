﻿using System;
using System.IO.Ports;
using System.Threading.Tasks;
using System.Windows;
using Optima_3_2.Enums;
using ModemControl;
using System.Threading;
using Client_New;
using System.Windows.Media;
using System.Diagnostics;

namespace Optima_3_2
{
    public partial class MainWindow : Window
    {

        private delegate void UpdateProgressBarDelegate(DependencyProperty dp, object value);
        private UpdateProgressBarDelegate updProgress;
        private RadioClient radioClient;
        private GSMClient GSMclient;
        private Stopwatch SendTime;
        private Stopwatch InitializationTime;

        private bool CodegramSended = false;
        private bool InitializationFinished = false;
        private bool CodegramNotSendedFlag = false;







        #region Initialize Features

        private void InitGSMClient()
        {
            propertiesPropGrid.OnComPortGSMChanged += PropertiesPropGrid_OnComPortGSMChanged;




            GSMclient = new GSMClient();

            GSMclient.OnSocketInitialized += GSMClient_OnSocketInitialized;
            GSMclient.OnSocketConnected += GSMClient_ConnectedToServer;
            GSMclient.OnClientRegistered += GSMClient_ClientRegistered;
            GSMclient.OnCodegramRecieved += GSMClient_CodegrammReceived;
            GSMclient.OnCodegramSended += GSMClient_OnCodegramSended;
            GSMclient.OnErrorOcured += GSMClient_ConnectionError;
            GSMclient.OnFindModem += GSMClient_OnFindModem;
            GSMclient.OnConfirmedSendCodegram += GSMClient_OnConfirmedSendCodegram;
            GSMclient.OnLostedConnection += GSMClient_OnLostedConnection;
            GSMclient.OnSomeErrorInitClient += GSMClient_OnSomeErrorInitClient;
            GSMclient.OnSomeErrorInitSocket += GSMClient_OnSomeErrorInitSocket;
            GSMclient.OnSomeErrorRead += GSMClient_OnSomeErrorRead;
            GSMclient.OnSomeErrorWrite += GSMClient_OnSomeErrorWrite;
            GSMclient.OnReadyToSendCodegram += GSMClient_OnReadyToSendCodegram;
            GSMclient.OnCloseSocket += GSMClient_OnCloseSocket;
            GSMclient.OnSomeMessageNotSended += GSMClient_OnSomeMessageNotSended;

            GSMclient.OnWriteString += OnWriteComand;
            GSMclient.OnWriteByte += OnWriteCodeg;
            GSMclient.OnReadString += OnReadComand;

            GSMclient.OpenPort(propertiesPropGrid.Local.Common.ComPort_GSM, 9600, Parity.None, 8, StopBits.One, ClientType.GSM);

            InitAllFlags();
        }

       

        private void OnReadComand(object sender, StringEventArgs e)
        {

            if (e.Data.Contains("SISR") == true)
            {
                string[] str = e.Data.Split(',');
                if (Convert.ToInt32(str[1]) <= 10)
                {
                    Dispatcher.Invoke(() =>
                    {
                        Log.AddTextToLog("r:  " + e.Data, Brushes.Red);
                    });
                }

            }
            else
            {
                Dispatcher.Invoke(() =>
                {
                    Log.AddTextToLog("r:  " + e.Data, Brushes.LightGreen);
                });
            }
            
        }

        private void OnWriteCodeg(object sender, BytesEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                Log.AddTextToLog("w:  " + e.Data, Brushes.White);
            });
        }

        private void OnWriteComand(object sender, StringEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                Log.AddTextToLog("w:  " + e.Data, Brushes.White);
            });
        }

        private void InitRadioClient()
        {
            propertiesPropGrid.OnComPortChanged += PropertiesPropGrid_OnComPortChanged;

            radioClient = new RadioClient();
            radioClient.OpenPort(propertiesPropGrid.Local.Common.ComPort_Radio, 9600, Parity.None, 8, StopBits.One, ClientType.Radio);
            radioClient.OnCodegramRecieved += ClientRadio_Codegram;
            radioClient.OnCodegramSended += ClientRadio_CodegramSended;
        }

        private void PropertiesPropGrid_OnComPortGSMChanged(object sender, string e)
        {
        
            EnableGSM(false);
            ErrorStatusModemCanvas.Visibility = Visibility.Collapsed;
            RegisteredStatusModemCanvas.Visibility = Visibility.Visible;
            SaveInitClientFlag(false);
       
            GSMclient.OpenPort(propertiesPropGrid.Local.Common.ComPort_GSM, 9600, Parity.None, 8, StopBits.One, ClientType.GSM);
            GSMclient.SendStartCommand();
        }


        private void PropertiesPropGrid_OnComPortChanged(object sender, string e)
        {
            radioClient.OpenPort(propertiesPropGrid.Local.Common.ComPort_Radio, 9600, Parity.None, 8, StopBits.One, ClientType.Radio);
        }

        #endregion





        #region GSM Events handlers

        private void GSMClient_OnSomeMessageNotSended(object sender, ByteEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                EnableGSM(true);
                ((Station)stations[e.Data - 1]).InitializationFlag = false;
            });
            if (CodegramSended == true)
                CodegramSended = false;
            else CodegramSended = true;
            CodegramNotSendedFlag = true;
        }

        private void GSMClient_OnReadyToSendCodegram(object sender, ByteEventArgs e)
        {

        }
        private void GSMClient_OnCloseSocket(object sender, ByteEventArgs e)
        {
            ((Station)stations[e.Data - 1]).InitializationFlag = false;
            ErrorStationsState(e.Data - 1);
            Dispatcher.Invoke(() =>
            {
                EnableGSM(true);
            });
        }

        private void GSMClient_OnSomeErrorWrite(object sender, StringEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                EnableGSM(true);
                ErrorStatusModemCanvas.Visibility = Visibility.Visible;
                ErrorStatusModemCanvas.Content = e.Data;
            });
            if (CodegramSended == true)
                CodegramSended = false;
            else CodegramSended = true;
        }

        private void GSMClient_OnSomeErrorRead(object sender, StringEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                EnableGSM(true);
                ErrorStatusModemCanvas.Visibility = Visibility.Visible;
                ErrorStatusModemCanvas.Content = e.Data;
            });
            if (CodegramSended == true)
                CodegramSended = false;
            else CodegramSended = true;
        }

        private void GSMClient_OnSomeErrorInitSocket(object sender, StringEventArgs e)
        {       
            Dispatcher.Invoke(() =>
            {
                EnableGSM(true);
                ErrorStatusModemCanvas.Visibility = Visibility.Visible;
                ErrorStatusModemCanvas.Content = e.Data;
            });
            if (CodegramSended == true)
                CodegramSended = false;
            else CodegramSended = true;
            InitializationFinished = true;
        }

        private void GSMClient_OnSomeErrorInitClient(object sender, StringEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                EnableGSM(true);
                ErrorStatusModemCanvas.Visibility = Visibility.Visible;
                ErrorStatusModemCanvas.Content = e.Data;
            });
        }

        private void GSMClient_CodegrammReceived(object sender, ByteEventArgs e)
        { 
            ProtocolOptima.StationCodegram r = (ProtocolOptima.StationCodegram)sender;
            CodegramRecievedAct(e.Data - 1, r);
            Thread.Sleep(100);
            if (CodegramSended == true)
                CodegramSended = false;
            else CodegramSended = true;
            InitializationFinished = true;          
            Dispatcher.Invoke(() =>
            {
                EnableGSM(true);
            });

            Dispatcher.Invoke(() =>
            {
                Log.AddTextToLog("Codegram decode", Brushes.Red);
            });
        }

        private void GSMClient_OnCodegramSended(object sender, ByteEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                EnableGSM(false);
            });

            Dispatcher.Invoke(() =>
            {
                Log.AddTextToLog("Codegram Sended", Brushes.Blue);
            });
        }

        private void GSMClient_OnConfirmedSendCodegram(object sender, ByteEventArgs e)
        {
            if (CodegramNotSendedFlag == true)
            { 
                CodegramNotSendedFlag = false;
                GSMclient.SendCloseSocket(e.Data);
                Thread.Sleep(400);
            }

            Dispatcher.Invoke(() =>
            {
                Log.AddTextToLog("Confirmed Send Codegramm", Brushes.Blue);
            });
        }

        private void GSMClient_OnLostedConnection(object sender, ByteEventArgs e)
        {      
            Dispatcher.Invoke(() =>
            {
                ((Station)stations[e.Data - 1]).InitializationFlag = false;
                SaveInitFlagOfStation(e.Data - 1, ((Station)stations[e.Data - 1]).InitializationFlag);
                EnableGSM(true);
                ErrorStatusModemCanvas.Visibility = Visibility.Visible;
                ErrorStatusModemCanvas.Content = "Conection losted on station " + e.Data;
            });
            if (CodegramSended == true)
                CodegramSended = false;
            else CodegramSended = true;
            GSMclient.SendCloseSocket(e.Data);
            Thread.Sleep(200);
            ErrorStationsState(e.Data-1);
        }

        private void GSMClient_ConnectionError(object sender, ByteEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                ((Station)stations[e.Data - 1]).InitializationFlag = false;
                SaveInitFlagOfStation(e.Data - 1, ((Station)stations[e.Data - 1]).InitializationFlag);
                EnableGSM(true);
                ErrorStatusModemCanvas.Visibility = Visibility.Visible;
                ErrorStatusModemCanvas.Content = "Connection error on station " + e.Data;
            });
            if (CodegramSended == true)
                CodegramSended = false;
            else CodegramSended = true;
            ErrorStationsState(e.Data-1);
            GSMclient.SendCloseSocket(e.Data);

        }

        private void GSMClient_ConnectedToServer(object sender, ByteEventArgs e)
        {           
            Thread.Sleep(2000);
            CommandType c = CommandType.POLL; Dispatcher.Invoke(() => { c = (CommandType)TypeOfCommand.SelectedItem; });
            Send(e.Data - 1, c, GSMclient);
            Dispatcher.Invoke(() =>
            {
                ((Station)stations[e.Data - 1]).InitializationFlag = true;
                SaveInitFlagOfStation(e.Data - 1, ((Station)stations[e.Data - 1]).InitializationFlag);
                DrawLine(e.Data - 1, 0);
            });
            
        }

        private void GSMClient_OnSocketInitialized(object sender, ByteEventArgs e)
        {

        }

        private void GSMClient_ClientRegistered()      
        {
            Dispatcher.Invoke(() =>
            {
                RegisteredStatusModemCanvas.Visibility = Visibility.Collapsed;
                ErrorStatusModemCanvas.Visibility = Visibility.Collapsed;
                EnableGSM(true);
                SaveInitClientFlag(true);
            });
        }

        private void GSMClient_OnFindModem()
        {
            Dispatcher.Invoke(() =>
            {
                ErrorStatusModemCanvas.Visibility = Visibility.Collapsed;
                labelStatusBar.Content = "Establishing a GSM connection";
            });
            GSMclient.InitializeClient();
        }

        private void CodegramRecievedAct(int i, ProtocolOptima.StationCodegram r)
        {
            Dispatcher.Invoke(() =>
            {
                ((Station)stations[i]).LedsColor(r.GLONASS_L1, r.GLONASS_L2, r.GPS_L1, r.GPS_L2, true, false, false, r.Galileo_L1, r.Galileo_L2, r.Beidou_L1, r.Beidou_L2, false, true, r.Existance_L1, r.Existance_L2);
                ((Station)stations[i]).FreqPC = r.FreqPC;

                DrawLine(i, r.SignalLevel);

                if (r.Existance_L1 == true || r.Existance_L2 == true)
                    ChoseRangeColor(i, StationStatus.Normal);
                if (r.Existance_L1 == false || r.Existance_L2 == false)
                    ChoseRangeColor(i, StationStatus.WithoutRadiation);
            });
        }

        private void Send(int i, CommandType mainComandType, Client client)
        {
            byte num = 0; Dispatcher.Invoke(() => { num = ((Station)stations[i]).NumVi; });
            string ipAddres = ""; Dispatcher.Invoke(() => { ipAddres = ((Station)stations[i]).IpAddress; });
            bool GPS_L1 = false; Dispatcher.Invoke(() => { GPS_L1 = (bool)GPSL1.IsChecked; });
            bool GPS_L2 = false; Dispatcher.Invoke(() => { GPS_L2 = (bool)GPSL2.IsChecked; });
            bool GLONASS_L1 = false; Dispatcher.Invoke(() => { GLONASS_L1 = (bool)GLONASSL1.IsChecked; });
            bool GLONASS_L2 = false; Dispatcher.Invoke(() => { GLONASS_L2 = (bool)GLONASSL2.IsChecked; });
            bool Galileo_L1 = false; Dispatcher.Invoke(() => { Galileo_L1 = (bool)GalileoL1.IsChecked; });
            bool Galileo_L2 = false; Dispatcher.Invoke(() => { Galileo_L2 = (bool)GalileoL2.IsChecked; });
            bool Beidou_L1 = false; Dispatcher.Invoke(() => { Beidou_L1 = (bool)BeidouL1.IsChecked; });
            bool Beidou_L2 = false; Dispatcher.Invoke(() => { Beidou_L2 = (bool)BeidouL2.IsChecked; });
            byte FreqPC = 1; Dispatcher.Invoke(() => { FreqPC = (byte)(switchFreqButton.SelectedIndex + 1); });

            switch (mainComandType)
            {
                case CommandType.POLL:
                    client.SendPoll(num, ipAddres);
                    break;
                case CommandType.ON:
                    client.SendOnOff(num, ipAddres, ProtocolOptima.CommandType.ON, GPS_L1, GPS_L2, GLONASS_L1, GLONASS_L2, Galileo_L1, Galileo_L2, Beidou_L1, Beidou_L2, FreqPC);
                    break;
                case CommandType.OFF:
                    client.SendOnOff(num, ipAddres, ProtocolOptima.CommandType.OFF, GPS_L1, GPS_L2, GLONASS_L1, GLONASS_L2, Galileo_L1, Galileo_L2, Beidou_L1, Beidou_L2, FreqPC);
                    break;
            }
        }


        #endregion





        #region Radio Events handlers

        private void ClientRadio_Codegram(object sender, StringEventArgs e)
        {
            ProtocolOptima.StationCodegram r = (ProtocolOptima.StationCodegram)sender;
            string IP = "";

            for (int i = 0; i < stations.Count; i++)
            {
                Dispatcher.Invoke(() => { IP = ((Station)stations[i]).IpAddress; });

                if (IP.Equals(e.Data))
                {
                    CodegramRecievedAct(i, r);
                }
            }
        }

        private void ClientRadio_CodegramSended(object sender, ByteEventArgs e)
        {
            Thread.Sleep(5000);
            Dispatcher.Invoke(() =>
            {
                SendSelectedButton.IsEnabled = true;
                SendAllButton.IsEnabled = true;
            });
        }



        #endregion





        #region Click region
        private void SendSelectedButton_Click(object sender, RoutedEventArgs e)
        {
            if (Radio.IsChecked == true)
                RadioClick();
            else if (GSM.IsChecked == true)
            {
                GSMClick();
            }
        }

        private void SendAllButton_Click(object sender, RoutedEventArgs e)
        {
            if (Radio.IsChecked == true)
                RadioClickAll();
            else if (GSM.IsChecked == true)
            {
                GSMClickAll();
            }

        }
        #endregion






        #region GSM Click

        private void GSMClick()
        {
            CommandType mainComandType = (CommandType)TypeOfCommand.SelectedItem;

            Task SendTask = Task.Factory.StartNew(() => {
                for (int i = 0; i < stations.Count; i++)
                {
                    if (Table.ListModemModel[i].IsActive)
                    {
                        Dispatcher.Invoke(() => { EnableGSM(false); });
                        GSMHandler(i, mainComandType);
                    }
                }
            });
        }

        private void GSMClickAll()
        {
            CommandType mainComandType = (CommandType)TypeOfCommand.SelectedItem;
            Task SendTask = Task.Factory.StartNew(() => {
                for (int i = 0; i < stations.Count; i++)
                {
                    Dispatcher.Invoke(() => { EnableGSM(false); });
                    GSMHandler(i, mainComandType);
                }           
            });
        }

        private int AmountOfResendCodegramm = 0;
        private int AmountOfReInitialization = 0;
        private void GSMHandler(int i, CommandType mainComandType)
        {
            if (((Station)stations[i]).InitializationFlag == true)
            {
                Thread.Sleep(55);
                Send(i, mainComandType, GSMclient);
                SendTime = new Stopwatch();
                SendTime.Start();
                CodegramSended = false;
                while (true)
                {
                    Thread.Sleep(100);
                    if (CodegramSended == true)
                    {
                        CodegramSended = false;
                        Thread.Sleep(500);
                        break;
                    }

                    if (SendTime.ElapsedMilliseconds > 40000)
                    {
                        if (AmountOfResendCodegramm < 2)
                        {
                            SendTime = new Stopwatch();
                            SendTime.Start();
                            Send(i, mainComandType, GSMclient);
                            AmountOfResendCodegramm++;
                        }
                        else { 
                            AmountOfResendCodegramm = 0;
                            GSMclient.SendCloseSocket((byte)(i + 1));
                            Thread.Sleep(200);
                            break;
                        }
                    }
                }
            }
            if (((Station)stations[i]).InitializationFlag == false)
            {
                GSMclient.InitializeSocket(((Station)stations[i]).NumVi, ((Station)stations[i]).IpAddress, ((Station)stations[i]).Port);
                InitializationTime = new Stopwatch();
                InitializationTime.Start();
                //InitializationFinished = false;
                while (true)
                {
                    Thread.Sleep(100);
                    if (InitializationFinished == true)
                    {
                        InitializationFinished = false;
                        Thread.Sleep(500);
                         break;
                    }


                    if (InitializationTime.ElapsedMilliseconds > 120000)
                    {
                            GSMclient.SendCloseSocket((byte) (i + 1));
                            Thread.Sleep(200);
                            InitializationTime = new Stopwatch();
                            InitializationTime.Start();
                            Thread.Sleep(100);
                            if (CodegramSended == true)
                                CodegramSended = false;
                            else CodegramSended = true;
                            break;
                    }
                }
            }
           
            //UpdateValueOfProgressBar((100 / stations.Count) * (i + 1), i);
            Dispatcher.Invoke(() => { DownPanel.IsEnabled = true; });
        }


        #endregion






        #region Radio Click
        private void RadioClickAll()
        {
            CommandType mainComandType = (CommandType)TypeOfCommand.SelectedItem;
            Task SendTask = Task.Factory.StartNew(() => {
                for (int i = 0; i < stations.Count; i++)
                {
                    Send(i, mainComandType, radioClient);
                }
            });
        }

        private void RadioClick()
        {           
            CommandType mainComandType = (CommandType)TypeOfCommand.SelectedItem;

            Task SendTask = Task.Factory.StartNew(() => {
                for (int i = 0; i < stations.Count; i++)
                {
                    if (Table.ListModemModel[i].IsActive)
                    {
                        Send(i, mainComandType, radioClient);
                    }
                }
            });
        }

        #endregion





        #region Interface actions

        private void EnableGSM(bool enable)
        {
            GSM.IsEnabled = enable;
            GSM.IsChecked = true;
            EnableSend(enable);
        }

        private void Radio_Checked(object sender, RoutedEventArgs e)
        {
            EnableSend(true);
        }

        private void EnableSend(bool enable)
        {
            SendSelectedButton.IsEnabled = enable;
            SendAllButton.IsEnabled = enable;
        }
        #endregion





        #region Save Flag

        private void InitAllFlags()
        {
           // if (ReadInitClientFlag() == false)
            {
                GSMclient.SendStartCommand();

                EnableGSM(false);
                
            }//else RegisteredStatusModemCanvas.Visibility = Visibility.Collapsed;

            //for (int i = 0; i < stations.Count; i++)
               // ((Station)stations[i]).InitializationFlag = ReadInitFlagOfStation(i);
        }

        private void SaveInitFlagOfStation(int j, bool flag)
        {
            INI.Write("Flags", "InitFlag" + j, flag.ToString());
        }

        private bool ReadInitFlagOfStation(int j)
        {
            bool flag = false;

            try
            {
                flag = Convert.ToBoolean(INI.ReadINI("Flags", "InitFlag" + j));
            }
            catch  { }
            return flag;
        }

        private void SaveInitClientFlag(bool flag)
        {
            INI.Write("Flags", "InitClientFlag", flag.ToString());
        }

        private bool ReadInitClientFlag()
        {
            bool flag = false;

            try
            {
                flag = Convert.ToBoolean(INI.ReadINI("Flags", "InitClientFlag"));
            }
            catch { }
            return flag;
        }
        #endregion





        #region Visualisation station

        private void DrawLine(int i, int level)
        {
           /* RasterMap.mapControl.RemoveObject(((Station)stations[i])?.StationLine);

            if(level == 0)
               ((Station)stations[i]).StationLine = RasterMap.mapControl.AddPolyline(((Station)stations[i]).DrawLines(new Mapsui.Geometries.Point(controlStation.Longitude, controlStation.Latitude)), Mapsui.Styles.Color.FromArgb(255, 255, 0, 0));
            if(level >= 1 && level < 10)
                ((Station)stations[i]).StationLine = RasterMap.mapControl.AddPolyline(((Station)stations[i]).DrawLines(new Mapsui.Geometries.Point(controlStation.Longitude, controlStation.Latitude)), Mapsui.Styles.Color.FromArgb(255, 255, 51, 0));
            if (level >= 10 && level < 30)
                ((Station)stations[i]).StationLine = RasterMap.mapControl.AddPolyline(((Station)stations[i]).DrawLines(new Mapsui.Geometries.Point(controlStation.Longitude, controlStation.Latitude)), Mapsui.Styles.Color.FromArgb(255, 255, 255, 0));
            if (level >=30)
                ((Station)stations[i]).StationLine = RasterMap.mapControl.AddPolyline(((Station)stations[i]).DrawLines(new Mapsui.Geometries.Point(controlStation.Longitude, controlStation.Latitude)), Mapsui.Styles.Color.FromArgb(255, 0, 255, 0));
        */
        }
        private void DrawRange(int i, Mapsui.Styles.Color color)
        {
            //((Station)stations[i]).StationPolyLine = RasterMap.mapControl.AddPolygon(((Station)stations[i]).DrawRange(), color);
        }

        private void ChoseRangeColor(int stationIndex, StationStatus stationStatus)
        {
            /*if ( RasterMap.IsLoaded == true)
            {
                switch (stationStatus)
                {
                    case StationStatus.Normal:
                        RasterMap.mapControl.RemoveObject(((Station)stations[stationIndex])?.StationPolyLine);
                        DrawRange(stationIndex, Mapsui.Styles.Color.FromArgb(100, 87, 175, 254));
                        break;
                    case StationStatus.WithoutRadiation:
                        RasterMap.mapControl.RemoveObject(((Station)stations[stationIndex])?.StationPolyLine);
                        DrawRange(stationIndex, Mapsui.Styles.Color.FromArgb(100, 255, 250, 87));
                        break;
                    case StationStatus.Error:
                        RasterMap.mapControl.RemoveObject(((Station)stations[stationIndex])?.StationPolyLine);
                        DrawRange(stationIndex, Mapsui.Styles.Color.FromArgb(100, 130, 130, 130));
                        break;
                    default:
                        RasterMap.mapControl.RemoveObject(((Station)stations[stationIndex])?.StationPolyLine);
                        DrawRange(stationIndex, Mapsui.Styles.Color.FromArgb(100, 135, 135, 135));
                        break;
                }
            }*/
        }

        private void ErrorStationsState(int j)
        {        
            Dispatcher.Invoke(() =>
            {
                ((Station)stations[j]).AllLedColor(Led.Gray);
               /// ChoseRangeColor(j, StationStatus.Error);
              //  DrawLine(j, 0);
            });
        }
        #endregion
    }
}
