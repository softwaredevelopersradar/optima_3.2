﻿using OptimaStationsProperties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using WpfMapControl;

namespace Optima_3_2
{
    public partial class MainWindow : Window
    {
        public List<ModemControl.ModemModel> listModem = new List<ModemControl.ModemModel>();

     /*   private void Table_OnAddRecord(object sender, ModemControl.TableEvents e)
        {
            if (i >= propertiesPropGrid.Local.Common.AmountOfStation) return;

            Location location = new Location(0, 0);
            Station station = new Station(location, propertiesPropGrid.Local.Common.Language, propertiesPropGrid.Local.Common.CoordinateView, i);
            
            AddEventsStationPropertyGrid(station);
            AddStationToScreen(station);
        }


        private void Table_OnChangeRecord(object sender, ModemControl.TableEvents e)
        {
           
            for (int j = 0; j < stations.Count; j++)
            {
                if (((Station)stations[j]).NumVi == e.Record.Number)
                { 
                    ((Station)stations[j]).MapObjectPropGrid.Visibility = Visibility.Visible;           
                    RasterMap.mapControl.NavigateTo(Mapsui.Projection.Mercator.FromLonLat(((Station)stations[j]).Longitude, ((Station)stations[j]).Latitude));
                }
                else
                    ((Station)stations[j]).MapObjectPropGrid.Visibility = Visibility.Collapsed;
            }
            cbGrid.Visibility = Visibility.Visible;
        }

       
        private void Table_OnDeleteRecord(object sender, ModemControl.TableEvents e)
        {
            DeleteOneStation(e.Record.Number);
        }     */

        private void Table_OnClearRecords(object sender, EventArgs e)
        {

            INI.DeleteKey("JStation", "Number");
            for (int j = 0; j < listModem.Count; j++)
            {
                RasterMap.mapControl.RemoveObject(((Station)stations[j])?.StaitionImage);
                RasterMap.mapControl.RemoveObject(((Station)stations[j])?.StationPolyLine);
                DeleteStationFromIniFile(listModem[j].Number);
            }
            Table.ListModemModel.Clear();
            listModem.Clear();
            stations.Clear();
            Table.ListModemModel = listModem;
            for (int j = 0; j < _AmountOfStation; j++)
            {
                RasterMap.ChangeIconOfMenuItemStation(j, Environment.CurrentDirectory + "/Resources/plus.png");
            }

        
        }
    }
}
