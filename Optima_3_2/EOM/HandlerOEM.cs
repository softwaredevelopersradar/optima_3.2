﻿using OEM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace Optima_3_2
{
    public partial class MainWindow : Window
    {
        
        private void OEMControlConnection_ButServerClick(object sender, RoutedEventArgs e)
        {            
            try
            {
                if (udpOEM != null && udpOEM.IsConnected)
                    DisconnectOEM();
                else
                    ConnectOEM();
            }

            catch
            { }            
        }

        private void ConnectPortOEM(object sender,EventArgs e)
        {
         
        }

        private void DisconnectPortOEM(object sender, EventArgs e)
        {
            
        }

        private void ConnectEchoOEM(object sender, EventArgs e)
        {
            
        }

        private void DisconnectEchoOEM(object sender, EventArgs e)
        {
            
        }

        private void TargetEchoOEM(object sender, EventArgs e)
        {
            
        }

        private void AnglesEchoOEM(object sender, AnglesEventArgs e)
        {           
          
        }

        private void VisionEchoOEM(object sender, AnglesEventArgs e)
        {
            
            
        }

        private void SetAngleOEM(float e)
        {
          
        }
    }
}
