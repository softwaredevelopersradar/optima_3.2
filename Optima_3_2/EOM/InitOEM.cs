﻿using OEM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Optima_3_2
{
    public partial class MainWindow : Window
    {
        public UdpOEM udpOEM;
         

        private void ConnectOEM()
        {
            if (udpOEM != null)
                DisconnectOEM();

            try
            {
                udpOEM = new UdpOEM();

                udpOEM.OnConnectPort += ConnectPortOEM;
                udpOEM.OnDisconnectPort += DisconnectPortOEM;
                udpOEM.OnConnectEcho += ConnectEchoOEM;
                udpOEM.OnDisconnectEcho += DisconnectEchoOEM;
                udpOEM.OnTargetEcho += TargetEchoOEM;
                udpOEM.OnAnglesEcho += AnglesEchoOEM;
                udpOEM.OnVisionEcho += VisionEchoOEM;


                udpOEM.Connect(propertiesPropGrid.Local.OEM.IpAddress, propertiesPropGrid.Local.OEM.Port, propertiesPropGrid.Local.OEM.IpAddress, propertiesPropGrid.Local.OEM.Port);


                udpOEM.SendConnect();
            }
            catch{ }

        }

        private void DisconnectOEM()
        {

            if (udpOEM != null)
            {
                udpOEM.Disconnect();

                udpOEM.OnConnectPort -= ConnectPortOEM;
                udpOEM.OnDisconnectPort -= DisconnectPortOEM;
                udpOEM.OnConnectEcho -= ConnectEchoOEM;
                udpOEM.OnDisconnectEcho -= DisconnectEchoOEM;
                udpOEM.OnTargetEcho -= TargetEchoOEM;
                udpOEM.OnAnglesEcho -= AnglesEchoOEM;
                udpOEM.OnVisionEcho -= VisionEchoOEM;

                udpOEM = null;
            }

        }

    }
}

