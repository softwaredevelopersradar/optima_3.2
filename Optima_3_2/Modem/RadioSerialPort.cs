﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO.Ports;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using ModemControl;
using ProtocolOptima;
using Optima_3_2.Enums;

namespace Optima_3_2
{
    public class RadioSerialPort : SerialPort
    {
        private const int DataSize = 54;
        private readonly byte[] _bufer = new byte[DataSize];

        public event EventHandler<String> ErrorReceived;


        public bool AVAILABILITY_MESSAGE = false;

        public RadioSerialPort(string port)
            : base()
        {
            base.PortName = port;
            base.DataBits = 8;
            base.StopBits = StopBits.Two;
            base.Parity = Parity.None;
            base.ReadTimeout = 1000;
            base.DataReceived += ModemSerialPort_DataReceived;
            base.ErrorReceived += ModemSerialPort_ErrorReceived;
            Open(port, 11500);
        }

        private void ModemSerialPort_ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            ErrorReceived(sender, e.ToString());
        }

        private void ModemSerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            AVAILABILITY_MESSAGE = true;
        }

        public void Open(string portName, int speed)
        {
            if (base.IsOpen)
            {
                base.Close();
            }
            base.BaudRate = speed;
            base.PortName = portName;
            base.Open();
        }

        public void Send(string IPLocal, byte addressLocal, string IPRemote, byte addressRemote, ProtocolOptima.CommandType commandType, bool? GPS_L1, bool? GPS_L2, bool? GLONASS_L1, bool? GLONASS_L2, bool? Radio_F1, bool? Radio_F2, bool? RadioGSM, byte FreqPC)
        {
            if (base.IsOpen == true)
            {
                 byte[] s = Protocol.FormPrefixByte(IPLocal, addressLocal, IPRemote, addressRemote, ProtocolOptima.CommandType.ON);
                 byte[] buff = Protocol.On_Off_Command(s,(bool)GPS_L1, (bool)GPS_L2, (bool)GLONASS_L1, (bool)GLONASS_L2, (bool)Radio_F1, (bool)Radio_F2, false, false, FreqPC);
                 base.Write(buff,0, buff.Length);             
            }
        }

        public void SendPoll(string IPLocal, byte addressLocal, string IPRemote, byte addressRemote)
        {
            byte[] s2 = Protocol.FormPrefixByte(IPLocal, addressLocal, IPRemote, addressRemote, ProtocolOptima.CommandType.POLL);
            base.Write(s2, 0, s2.Length);
        }


        public StationCodegram Read()
        {
            byte[] buff = new byte[42];
            base.Read(buff, 0, 42);

            return Protocol.DecryptionCodegram(buff);
        }


        public void Disconect()
        {
            base.Close();
        }
    }
}