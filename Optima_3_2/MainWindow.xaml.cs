﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfMapControl;
using CoordFormatLib;
using System.Drawing;
using System.Collections;
using Mapsui.Providers;
using GeoUtility.GeoSystem;
using OptimaStationsProperties.Model;
using OptimaStationsProperties;
using OptimaSettingsProperties.Model;
using ModemControl;
using System.IO;
using OptimaControlStationProperties;

namespace Optima_3_2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Station> stations;
        private ControlStationObject controlStation;
        private string stationName;
        MapObjectStyle _stationStyle;
        MapObjectStyle _controlStationStyle;

        private const int _AmountOfStation = 9;

        public MainWindow()
        {
            InitializeComponent();
      
            stations = new List<Station>();

            InitSettings();     
            InitTable();
            InitRasterMap();
         
            ReadIni();

            OpenMap();

            TableToggleButton.IsChecked = true;
            DownPanelToggleButton.IsChecked = true;


            InitGSMClient();
            InitRadioClient();
        }

        #region Initialize Features
        private void InitRasterMap()
        {
            _stationStyle = RasterMap.mapControl.LoadObjectStyle(propertiesPropGrid.Local.Map.ImageStation, new Mapsui.Styles.Offset(0, 35), scale: 0.32, new Mapsui.Styles.Offset(0, -40));
            _controlStationStyle = RasterMap.mapControl.LoadObjectStyle(propertiesPropGrid.Local.Map.ImageASY, new Mapsui.Styles.Offset(0, 35), scale: 0.5, new Mapsui.Styles.Offset(0, -40));
            RasterMap.OnMapOpen += RasterMap_OnMapOpen;
            RasterMap.Projection = MapProjection.Mercator;
            RasterMap.MenuItemStationClick += RasterMap_MenuItemStationClick;
            RasterMap.MenuItemMainStationClick += RasterMap_MenuItemMainStationClick;
            RasterMap.MenuItemDeleteAllStationClick += Table_OnClearRecords;
            RasterMap.PathMap = Properties.Settings.Default.FileMap;
            RasterMap.PathMapTile = Properties.Settings.Default.FolderMapTile;
  

            for (int i = 0; i < _AmountOfStation; i++)
                RasterMap.ChangeIconOfMenuItemStation(i, Environment.CurrentDirectory + "/Resources/plus.png");
        }

        private void InitTable()
        {
            Table.SetTranslation((Language)propertiesPropGrid.Local.Common.Language);
        }

        private void InitSettings()
        {
            propertiesPropGrid.Local.Common.CoordinateView = OptimaSettingsProperties.Model.ViewCoord.DMSs;
            InitializeLangugeDictionary(propertiesPropGrid.Local.Common.Language);

            ApplySettings();
        }

        private void OpenMap()
        {
            if (RasterMap.PathMap != null)
            {
                try { RasterMap.OpenMap(); }
                catch (Exception) { System.Windows.MessageBox.Show("Map not found"); }
            }
        }

        #endregion

        #region Language
        private void PropertiesPropGrid_OnLanguageChanged(object sender, OptimaSettingsProperties.Model.Languages e)
        {
            InitializeLangugeDictionary(e);
            ReDrawAllStation();

            for (int j = 0; j < stations.Count; j++)
            {
                ((Station)stations[j]).SetLanguage(propertiesPropGrid.Local.Common.Language);
            }

            Table.SetTranslation((ModemControl.Language)propertiesPropGrid.Local.Common.Language);
            Properties.Settings.Default.Language = e;
            Properties.Settings.Default.Save();
        }

        private void InitializeLangugeDictionary(OptimaSettingsProperties.Model.Languages languages)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (languages)
                {
                    case OptimaSettingsProperties.Model.Languages.EN:
                        dict.Source = new Uri("/Optima_3_2;component/Language/StringResource.EN.xaml",
                                      UriKind.Relative);
                        stationName = "Station";

                        break;
                    case OptimaSettingsProperties.Model.Languages.RU:
                        dict.Source = new Uri("/Optima_3_2;component/Language/StringResource.RU.xaml",
                                           UriKind.Relative);
                        stationName = "Станция";
                        break;
                    case OptimaSettingsProperties.Model.Languages.AZ:
                        dict.Source = new Uri("/Optima_3_2;component/Language/StringResource.AZ.xaml",
                                           UriKind.Relative);
                        stationName = "Stsnsiyası";
                        break;
                }

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception)
            { }
        }
        #endregion

        #region Events Menu Items
        private void RasterMap_MenuItemMainStationClick(object sender, RoutedEventArgs e)
        {
            RasterMap.mapControl.RemoveObject(controlStation?.StaitionImage);
            
            controlStation = new ControlStationObject(new Location(RasterMap.ClickCoord.Longitude, RasterMap.ClickCoord.Latitude), propertiesPropGrid.Local.Common.Language, propertiesPropGrid.Local.Common.CoordinateView, "Самый главный");
            controlStation.OnApplyButtonClick += ControlStationPropertyGrid_OnApplyButtonClick;
            controlStation.OnDeleteButtonClick += ControlStationPropertyGrid_OnDeleteButtonClick;
    

            cbGrid.Children.Add(controlStation.AbstractPropertyGrid);
            cbGrid.Visibility = Visibility.Visible;
            RasterMap.mapControl.IsEnabled = false;
        }


        private void RasterMap_MenuItemStationClick(object sender, int e)
        {
            cbGrid.Visibility = Visibility.Visible;
            if (stations.Count >= 0)
            {
                for (int j = 0; j < stations.Count; j++)
                {
                     ((Station)stations[j]).ChangePropertyGridVisibility(false);
                }
                for (int j = 0; j < stations.Count; j++)
                {
                    if (((Station)stations[j]).NumVi == e)
                    {                      
                        //RasterMap.mapControl.NavigateTo(Mapsui.Projection.Mercator.FromLonLat(((Station)stations[j]).Longitude, ((Station)stations[j]).Latitude));

                        ((Station)stations[j]).ChangePropertyGridVisibility(true); ;
                        return;
                    }
                }
            }

            Station station = new Station(new Location(RasterMap.ClickCoord.Longitude, RasterMap.ClickCoord.Latitude), propertiesPropGrid.Local.Common.Language, propertiesPropGrid.Local.Common.CoordinateView, e);

            AddEventsStationPropertyGrid(station);
            AddStationToScreen(station);
            RasterMap.ChangeIconOfMenuItemStation(e-1, Environment.CurrentDirectory + "/Resources/update.png");
            return;
        }

        #endregion

        #region Events Map
        private void RasterMap_OnMapOpen(object sender, string e)
        {
            if (stations != null)
                if (stations.Count > 0)
                    for (int j = 0; j < stations.Count; j++)
                        DrawStation(j);
            propertiesPropGrid.Local.Common.FileMap = e;
            Properties.Settings.Default.FileMap = e;
            Properties.Settings.Default.Save();

            if (controlStation != null)
                DrawControlStation();
        }

        private void OnMapTilesPathChanged(object sender, string e)
        {
            RasterMap.PathMapTile = e;
        }

        private void AddEventsStationPropertyGrid(Station station)
        {
            station.OnApplyButtonClick += OnApplyButtonClick;
            station.OnDeleteButtonClick += OnDeleteButtonClick;
            station.OnUpdateModemModel += Station_OnUpdateModemModel;
        }

        private void AddStationToScreen(Station station)
        {
            cbGrid.Children.Add(station.AbstractPropertyGrid);
            cbGrid.Visibility = Visibility.Visible;
            stations.Add(station);

            RasterMap.mapControl.IsEnabled = false;
        }
        #endregion

        #region Events Station Settings

        private void Station_OnUpdateModemModel(int j, ModemModel e)
        {
            for (int k = 0; k < listModem.Count; k++)
                if (listModem[k].Number == e.Number)
                {
                    listModem[k] = e;
                    Table.ListModemModel = listModem;
                    return;
                }

            listModem.Add(e);
            Table.ListModemModel = listModem;
        }

        private void OnDeleteButtonClick(object sender, PropertiesOfMapObject e)
        {          
            DeleteOneStation(e.Common.Num);
            cbGrid.Visibility = Visibility.Collapsed;
            RasterMap.mapControl.IsEnabled = true;
        }
        private void OnApplyButtonClick(object sender, PropertiesOfMapObject e)
        {
            int j = GetIndexOfStation(e.Common.Num);

            RasterMap.mapControl.RemoveObject(((Station)stations[j])?.StaitionImage);
            RasterMap.mapControl.RemoveObject(((Station)stations[j])?.StationPolyLine);

            if (RasterMap.mapControl.IsMapLoaded == true)
                DrawStation((Station)stations[j], e.Common.Num);

            ((Station)stations[j]).CreateModemModel();
            ((Station)stations[j]).UpdateModemModel();
            RasterMap.ChangeIconOfMenuItemStation(((Station)stations[j]).NumVi - 1, Environment.CurrentDirectory + "/Resources/update.png");

            cbGrid.Visibility = Visibility.Collapsed;
            SaveStation((Station)stations[j], ((Station)stations[j]).NumVi);
         
            stations.Sort(SortStation_Massive);
            listModem.Sort(SortStations_Table);
            Table.ListModemModel = listModem;

            RasterMap.mapControl.IsEnabled = true;
        }

        private static int SortStations_Table(ModemModel modemModelX, ModemModel modemModelY)
        {
            if (modemModelX.Number == modemModelY.Number)
                return 0;
            int retval = modemModelX.Number.CompareTo(modemModelY.Number);
            return retval;

        }

        private static int SortStation_Massive(Station station1, Station station2)
        {
            if (station1.NumVi == station2.NumVi)
                return 0;
            int retval = station1.NumVi.CompareTo(station2.NumVi);
            return retval;
        }

        private void ControlStationPropertyGrid_OnDeleteButtonClick(object sender, OptimaControlStationProperties.Model.ControlStationProperties e)
        {
            RasterMap.mapControl.RemoveObject(controlStation?.StaitionImage);
            cbGrid.Visibility = Visibility.Collapsed;
            RasterMap.mapControl.IsEnabled = true;
        }

        private void ControlStationPropertyGrid_OnApplyButtonClick(object sender, OptimaControlStationProperties.Model.ControlStationProperties e)
        {
            RasterMap.mapControl.RemoveObject(controlStation?.StaitionImage);

            if (RasterMap.mapControl.IsMapLoaded == true)
                DrawControlStation();

            SaveControlStation();
            cbGrid.Visibility = Visibility.Collapsed;
            RasterMap.mapControl.IsEnabled = true;
        }


        private int GetIndexOfStation(byte num)
        {
            for (int j = 0; j < stations.Count; j++)
            {
                if (((Station)stations[j]).NumVi == num)
                {
                    return j;
                }
            }
            return 0;
        }

        private void DeleteOneStation(byte number)
        {
            int j = GetIndexOfStation(number);
            if (j >= stations.Count-1)
                return;

            RasterMap.mapControl.RemoveObject(((Station)stations[j])?.StaitionImage);
            RasterMap.mapControl.RemoveObject(((Station)stations[j])?.StationPolyLine);

            stations.RemoveAt(j);
            listModem.RemoveAt(j);
            Table.ListModemModel = listModem;
            DeleteStationFromIniFile(number);
            RasterMap.ChangeIconOfMenuItemStation(number - 1, Environment.CurrentDirectory + "/Resources/plus.png");

        }
        #endregion

        #region Ini file

        IniFile INI;

        private void OpenIniFile()
        {
            try { INI = new IniFile("config.ini"); }
            catch (Exception) { System.Windows.MessageBox.Show("Ini file not found"); }
        }
        private void ReadIni()
        {
            OpenIniFile();

            int count = 0;
                count = 9;     
            for (int j = 1; j <= count; j++)
            {
                if (INI.KeyExists("LatitudeStation" + j, "Latitude") && INI.KeyExists("LongitudeStation" + j, "Longitude"))
                {
                    Location location = new Location(double.Parse(INI.ReadINI("Longitude", "LongitudeStation" + j)), double.Parse(INI.ReadINI("Latitude", "LatitudeStation" + j)));

                   Station station = new Station(location, propertiesPropGrid.Local.Common.Language, propertiesPropGrid.Local.Common.CoordinateView, j);

                    if (INI.KeyExists("DistanceStation" + j, "Distance") && INI.KeyExists("DirectionStation" + j, "Direction"))
                    {
                        station.Distance = double.Parse(INI.ReadINI("Distance", "DistanceStation" + j));
                        station.Direction = double.Parse(INI.ReadINI("Direction", "DirectionStation" + j));
                    }

                    if (INI.KeyExists("IpAddressStation" + j, "IpAddress"))
                        station.IpAddress = INI.ReadINI("IpAddress", "IpAddressStation" + j);
                    if (INI.KeyExists("PortStation" + j, "Port"))
                        station.Port = int.Parse(INI.ReadINI("Port", "PortStation" + j));
                    if (INI.KeyExists("NumStation" + j, "Number"))
                        station.NumVi = byte.Parse(INI.ReadINI("Number", "NumStation" + j));
                    if (INI.KeyExists("NoteStation" + j, "Note"))
                        station.Note = INI.ReadINI("Note", "NoteStation" + j);

                    
                    cbGrid.Children.Add(station.AbstractPropertyGrid);
                           
                    AddEventsStationPropertyGrid(station);              
                    station.UpdateModemModel();
                    stations.Add(station);
                    RasterMap.ChangeIconOfMenuItemStation(station.NumVi - 1, Environment.CurrentDirectory + "/Resources/update.png");
                }
                stations.Sort(SortStation_Massive);
            }

            if (INI.KeyExists("Latitude", "ControlStation") && INI.KeyExists("Longitude", "ControlStation"))
            {
                Location loc = new Location(double.Parse(INI.ReadINI("ControlStation", "Longitude")), double.Parse(INI.ReadINI("ControlStation", "Latitude")));

                controlStation = new ControlStationObject(loc, propertiesPropGrid.Local.Common.Language, propertiesPropGrid.Local.Common.CoordinateView, INI.ReadINI("ControlStation", "Name"));
                RasterMap.GNSSPosition = loc;
                if (INI.KeyExists("Note", "ControlStation"))
                    controlStation.Note = INI.ReadINI("ControlStation", "Note");
                controlStation.OnApplyButtonClick += ControlStationPropertyGrid_OnApplyButtonClick;
                controlStation.OnDeleteButtonClick += ControlStationPropertyGrid_OnDeleteButtonClick;
            }

            cbGrid.Visibility = Visibility.Collapsed;
        }

        private void SaveStation(Station station, int j)
        {
            INI.Write("Latitude", "LatitudeStation" + j, station.Latitude.ToString());
            INI.Write("Longitude", "LongitudeStation" + j, station.Longitude.ToString());
            INI.Write("Distance", "DistanceStation" + j, station.Distance.ToString());
            INI.Write("Direction", "DirectionStation" + j, station.Direction.ToString());
            INI.Write("IpAddress", "IpAddressStation" + j, station.IpAddress);
            INI.Write("Port", "PortStation" + j, station.Port.ToString());
            INI.Write("Number", "NumStation" + j, station.NumVi.ToString());
            INI.Write("Note", "NoteStation" + j, station.Note);          
        }

        
        private void SaveControlStation()
        {
            INI.Write("ControlStation", "Latitude", controlStation.Latitude.ToString());
            INI.Write("ControlStation", "Longitude", controlStation.Longitude.ToString());
            INI.Write("ControlStation", "Name", controlStation.Name.ToString());
            INI.Write("ControlStation", "Note", controlStation.Note.ToString());          
        }

        private void DeleteStationFromIniFile( int j)
        {
         //   j = j + 1;
            INI.DeleteKey("LatitudeStation" + j, "Latitude");
            INI.DeleteKey("LongitudeStation" + j, "Longitude");
            INI.DeleteKey("DistanceStation" + j, "Distance");
            INI.DeleteKey("DirectionStation" + j, "Direction");
            INI.DeleteKey("IpAddressStation" + j, "IpAddress");
            INI.DeleteKey("PortStation" + j, "Port");
            INI.DeleteKey("NumStation" + j, "Number");
            INI.DeleteKey("NoteStation" + j, "Note");
        }
        #endregion

        #region Settings
        private void ApplySettings()
        {
            propertiesPropGrid.Local.Common.CoordinateView = Properties.Settings.Default.CoordinateView;
            propertiesPropGrid.Local.Common.FileMap = Properties.Settings.Default.FileMap;
            propertiesPropGrid.Local.Common.FolderMapTiles = Properties.Settings.Default.FolderMapTile;
            propertiesPropGrid.Local.Common.Language = Properties.Settings.Default.Language;
            propertiesPropGrid.Local.Common.ComPort_GSM = Properties.Settings.Default.ComPortASY;
            propertiesPropGrid.Local.Common.ComPort_Radio = Properties.Settings.Default.ComPort;
            propertiesPropGrid.Local.BRD.Address = Properties.Settings.Default.Addres;
            propertiesPropGrid.Local.BRD.Availability = Properties.Settings.Default.AvailabilityBRD;
            propertiesPropGrid.Local.BRD.ComPort = Properties.Settings.Default.ComPort;
            propertiesPropGrid.Local.BRD.TransportAngle = Properties.Settings.Default.TransportAngle;
            propertiesPropGrid.Local.BRD.PortSpeed = Properties.Settings.Default.PortSpeedBRD;
            propertiesPropGrid.Local.OEM.Availability = Properties.Settings.Default.AvailabilityDM;
            propertiesPropGrid.Local.OEM.IpAddress = Properties.Settings.Default.IpAddress;
            propertiesPropGrid.Local.OEM.Port = Properties.Settings.Default.Port;
            propertiesPropGrid.Local.OEM.CorrectionAngle = Properties.Settings.Default.CorrectionAngle; 
            propertiesPropGrid.Local.Map.ImageASY = Properties.Settings.Default.ImageASY;
            propertiesPropGrid.Local.Map.ImageStation = Properties.Settings.Default.ImageStation;
        }

        private void PropertiesPropGrid_OnApplyButtonClick(object sender, PropertiesOfOPY e)
        {
            Properties.Settings.Default.IpAddress = e.OEM.IpAddress;
            Properties.Settings.Default.Port = e.OEM.Port;
            Properties.Settings.Default.AvailabilityDM = e.OEM.Availability;
            Properties.Settings.Default.Language = e.Common.Language;
            Properties.Settings.Default.CoordinateView = e.Common.CoordinateView;
            Properties.Settings.Default.FileMap = e.Common.FileMap;
            Properties.Settings.Default.FolderMapTile = e.Common.FolderMapTiles;
            Properties.Settings.Default.ComPortASY = e.Common.ComPort_GSM;
            Properties.Settings.Default.ComPort = e.Common.ComPort_Radio;
            Properties.Settings.Default.AvailabilityBRD = e.BRD.Availability;
            Properties.Settings.Default.ComPort = e.BRD.ComPort;
            Properties.Settings.Default.TransportAngle = e.BRD.TransportAngle;
            Properties.Settings.Default.Addres = e.BRD.Address;
            Properties.Settings.Default.PortSpeedBRD = e.BRD.PortSpeed;
            Properties.Settings.Default.CorrectionAngle = e.OEM.CorrectionAngle;
            Properties.Settings.Default.ImageASY = e.Map.ImageASY;
            Properties.Settings.Default.ImageStation = e.Map.ImageStation;
            Properties.Settings.Default.Save();
        }
        #endregion

        #region Draw stations      
        private void DrawControlStation()
        {
            Mapsui.Geometries.Point mainPoint = Mapsui.Projection.Mercator.FromLonLat(controlStation.Longitude, controlStation.Latitude);       
            controlStation.StaitionImage = RasterMap.mapControl.AddMapObject(_controlStationStyle, controlStation.Name, mainPoint);
        }
        private void DrawStation(Station station, int num)
        {
            Mapsui.Geometries.Point mainPoint = Mapsui.Projection.Mercator.FromLonLat(station.Longitude, station.Latitude);
            station.StaitionImage = RasterMap.mapControl.AddMapObject(_stationStyle, stationName + " " + (num).ToString(), mainPoint);
        }
        private void DrawStation(int j)
        {
            Mapsui.Geometries.Point mainPoint = Mapsui.Projection.Mercator.FromLonLat(((Station)stations[j]).Longitude, ((Station)stations[j]).Latitude);
            ((Station)stations[j]).StaitionImage = RasterMap.mapControl.AddMapObject(_stationStyle, stationName + " " + (((Station)stations[j]).NumVi).ToString(), mainPoint);           
        }

        private void ReDrawAllStation()
        {
            for (int j = 0; j < stations.Count; j++)
            {
                ReDrawStation(j);
            }
        }

        private void ReDrawStation(int j)
        {
            RasterMap.mapControl.RemoveObject(((Station)stations[j])?.StaitionImage);
            DrawStation(j);
        }
        #endregion

        #region FormatCordinate
        private void OnCoordinateFormatChanged(object sender, OptimaSettingsProperties.Model.ViewCoord e)
        {
            switch (e)
            {
                case OptimaSettingsProperties.Model.ViewCoord.Dd:
                    RasterMap.FormatViewCoord = FormatCoord.DD;                   
                    break;
                case OptimaSettingsProperties.Model.ViewCoord.DMm:
                    RasterMap.FormatViewCoord = FormatCoord.DD_MM_mm;
                    break;
                case OptimaSettingsProperties.Model.ViewCoord.DMSs:
                    RasterMap.FormatViewCoord = FormatCoord.DD_MM_SS_ss;
                    break;
            }


            if (stations != null && stations.Count > 0)
            {
                
                for (int j = 0; j < stations.Count; j++)
                {
                    ((Station)stations[j]).SetCoordView(e);
                }
            }
        }
        #endregion

        #region VisibilityPanels
      private  GridLength h;
        private void TableToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            RowTable.Height = h;
        }

        private void TableToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            h = RowTable.Height;
            RowTable.Height = new GridLength(0, GridUnitType.Pixel);
        }

        private void DownPanelToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            RowDownPanel.Height = new GridLength(54, GridUnitType.Pixel);
        }

        private void DownPanelToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            RowDownPanel.Height = new GridLength(0, GridUnitType.Pixel);
        }

      private  GridLength w = new GridLength(300, GridUnitType.Pixel);

        private void SettingToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            ColumnSettings.Width = w;
        }

        private void SettingToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            w = ColumnSettings.Width;
            ColumnSettings.Width = new GridLength(0, GridUnitType.Pixel);
        }

        #endregion

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (comBRD != null)
                    comBRD.SendSetAngle(0, (short)propertiesPropGrid.Local.BRD.TransportAngle, 0);
            }
            catch { }


            System.Windows.Threading.Dispatcher.ExitAllFrames();
        }
        private void LogToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            Log.Visibility = Visibility.Visible;
        }

        private void LogToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {         
            Log.Visibility = Visibility.Collapsed;
        }

    }   
  }


