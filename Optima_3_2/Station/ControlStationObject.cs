﻿using CoordFormatLib;
using Mapsui.Providers;
using OptimaControlStationProperties;
using OptimaControlStationProperties.Model;
using System;
using System.Linq;
using System.Windows.Controls;
using WpfMapControl;

namespace Optima_3_2
{
    public class ControlStationObject : AbstractStation
    {
        public event EventHandler<ControlStationProperties> OnApplyButtonClick = (sender, obj) => { };
        public event EventHandler<ControlStationProperties> OnDeleteButtonClick = (sender, obj) => { };
        public override double Latitude
        {
            get
            {
                return ((ControlStation)AbstractPropertyGrid).Local.LocationInfo.Latitude;
            }
            set
            {
                ((ControlStation)AbstractPropertyGrid).Local.LocationInfo.Latitude = value;
            }
        }
        public override double Longitude
        {
            get
            {
                return ((ControlStation)AbstractPropertyGrid).Local.LocationInfo.Longitude;
            }
            set
            {
                ((ControlStation)AbstractPropertyGrid).Local.LocationInfo.Longitude = value;
            }
        }
        public string Name
        {
            get
            {
                return ((ControlStation)AbstractPropertyGrid).Local.Common.Name;
            }
            set
            {
                ((ControlStation)AbstractPropertyGrid).Local.Common.Name = value;
            }
        }
        public override string Note
        {
            get
            {
                return ((ControlStation)AbstractPropertyGrid).Local.Common.Note;
            }
            set
            {
                ((ControlStation)AbstractPropertyGrid).Local.Common.Note = value;
            }
        }


        public ControlStationObject(Location location, OptimaSettingsProperties.Model.Languages languages, OptimaSettingsProperties.Model.ViewCoord viewCoord, string name)
        {
            AbstractPropertyGrid = new ControlStation();
            CreateMapObjectPropertyGrid(location.Latitude, location.Longitude, viewCoord, name);

            SetLanguage(languages);
            SetCoordView(viewCoord);
            ((ControlStation)AbstractPropertyGrid).OnApplyButtonClick += ControlStationObject_OnApplyButtonClick;
            ((ControlStation)AbstractPropertyGrid).OnDeleteButtonClick += ControlStationObject_OnDeleteButtonClick;
        }

        private void ControlStationObject_OnDeleteButtonClick(object sender, ControlStationProperties e)
        {
            OnDeleteButtonClick(sender, e);
        }

        private void ControlStationObject_OnApplyButtonClick(object sender, OptimaControlStationProperties.Model.ControlStationProperties e)
        {
            OnApplyButtonClick(sender, e);
        }

        protected void PutStationPropGridOnWindow()
        {
            Grid.SetRow(((ControlStation)AbstractPropertyGrid), 1);
            Grid.SetZIndex(((ControlStation)AbstractPropertyGrid), 1);
            Grid.SetColumn(((ControlStation)AbstractPropertyGrid), 0);
        }

        public override void SetLanguage(OptimaSettingsProperties.Model.Languages languages)
        {
            ((ControlStation)AbstractPropertyGrid).Local.Common.Language = (OptimaControlStationProperties.Model.Languages)languages;
        }

        public void TranslateCoordinate()
        {
            VCFormat VCoordSpooffing = new VCFormat(((ControlStation)AbstractPropertyGrid).Local.LocationInfo.Latitude, ((ControlStation)AbstractPropertyGrid).Local.LocationInfo.Longitude);

            switch (((ControlStation)AbstractPropertyGrid).Local.LocationInfo.ViewCoordField)
            {
                case OptimaControlStationProperties.Model.ViewCoord.DMm:

                    if (Math.Round(Math.Abs(VCoordSpooffing.coordDegMin.Latitude.Minute), 4) >= 60)
                        ((ControlStation)AbstractPropertyGrid).Local.LocationInfo.LatitudeStr = (VCoordSpooffing.coordDegMin.Latitude.Degree + 1).ToString() + "°" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSpooffing.coordDegMin.Latitude.Minute) - 60, 4).ToString()) + "'";
                    else ((ControlStation)AbstractPropertyGrid).Local.LocationInfo.LatitudeStr = (VCoordSpooffing.coordDegMin.Latitude.Degree).ToString() + "°" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSpooffing.coordDegMin.Latitude.Minute), 4).ToString()) + "'";

                    if (Math.Round(Math.Abs(VCoordSpooffing.coordDegMin.Longitude.Minute), 4) >= 60)
                        ((ControlStation)AbstractPropertyGrid).Local.LocationInfo.LongitudeStr = (VCoordSpooffing.coordDegMin.Longitude.Degree + 1).ToString() + "°" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSpooffing.coordDegMin.Longitude.Minute) - 60, 4).ToString()) + "'";
                    else ((ControlStation)AbstractPropertyGrid).Local.LocationInfo.LongitudeStr = (VCoordSpooffing.coordDegMin.Longitude.Degree).ToString() + "°" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSpooffing.coordDegMin.Longitude.Minute), 4).ToString()) + "'";
                   
                    break;
                case OptimaControlStationProperties.Model.ViewCoord.DMSs:
                    if (Math.Round(Math.Abs(VCoordSpooffing.coordDegMinSec.Latitude.Second)) >= 60)
                        ((ControlStation)AbstractPropertyGrid).Local.LocationInfo.LatitudeStr = (VCoordSpooffing.coordDegMinSec.Latitude.Degree).ToString() + "°" + (Math.Abs(VCoordSpooffing.coordDegMinSec.Latitude.Minute) + 1).ToString() + "'" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSpooffing.coordDegMinSec.Latitude.Second) - 60).ToString()) + "\"";
                    else ((ControlStation)AbstractPropertyGrid).Local.LocationInfo.LatitudeStr = (VCoordSpooffing.coordDegMinSec.Latitude.Degree).ToString() + "°" + (Math.Abs(VCoordSpooffing.coordDegMinSec.Latitude.Minute)).ToString() + "'" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSpooffing.coordDegMinSec.Latitude.Second)).ToString()) + "\"";

                    if (Math.Round(Math.Abs(VCoordSpooffing.coordDegMinSec.Longitude.Second)) >= 60)
                        ((ControlStation)AbstractPropertyGrid).Local.LocationInfo.LongitudeStr = (VCoordSpooffing.coordDegMinSec.Longitude.Degree).ToString() + "°" + (Math.Abs(VCoordSpooffing.coordDegMinSec.Longitude.Minute) + 1).ToString() + "'" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSpooffing.coordDegMinSec.Longitude.Second) - 60).ToString()) + "\"";
                    else ((ControlStation)AbstractPropertyGrid).Local.LocationInfo.LongitudeStr = (VCoordSpooffing.coordDegMinSec.Longitude.Degree).ToString() + "°" + (Math.Abs(VCoordSpooffing.coordDegMinSec.Longitude.Minute)).ToString() + "'" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSpooffing.coordDegMinSec.Longitude.Second)).ToString()) + "\"";

                    break;
                case OptimaControlStationProperties.Model.ViewCoord.Dd:
                    ((ControlStation)AbstractPropertyGrid).Local.LocationInfo.LatitudeStr = CutRightZerosOfString(Math.Round(VCoordSpooffing.coordDeg.Latitude.Degree, 6).ToString()) + "°";
                    ((ControlStation)AbstractPropertyGrid).Local.LocationInfo.LongitudeStr = CutRightZerosOfString(Math.Round(VCoordSpooffing.coordDeg.Longitude.Degree, 6).ToString()) + "°";
                    break;
                default: break;
            }
        }

        public override void SetCoordView(OptimaSettingsProperties.Model.ViewCoord viewCoord)
        {
            ChangeCoordinateViewForStationsPropGrid(viewCoord);
            TranslateCoordinate();

        }

        private void CreateMapObjectPropertyGrid(double latitude, double longitude, OptimaSettingsProperties.Model.ViewCoord viewCoord, string name)
        {
            ChangeCoordinateViewForStationsPropGrid(viewCoord);
            ((ControlStation)AbstractPropertyGrid).Local.LocationInfo.Latitude = Math.Round(latitude, 4);
            ((ControlStation)AbstractPropertyGrid).Local.LocationInfo.Longitude = Math.Round(longitude, 4);
            ((ControlStation)AbstractPropertyGrid).Local.Common.Name = name;
            PutStationPropGridOnWindow();
        }
   

        private void ChangeCoordinateViewForStationsPropGrid(OptimaSettingsProperties.Model.ViewCoord viewCoord)
        {
            switch (viewCoord)
            {
                case OptimaSettingsProperties.Model.ViewCoord.Dd:
                    ((ControlStation)AbstractPropertyGrid).Local.LocationInfo.ViewCoordField = OptimaControlStationProperties.Model.ViewCoord.Dd;
                    break;
                case OptimaSettingsProperties.Model.ViewCoord.DMm:
                    ((ControlStation)AbstractPropertyGrid).Local.LocationInfo.ViewCoordField = OptimaControlStationProperties.Model.ViewCoord.DMm;
                    break;
                case OptimaSettingsProperties.Model.ViewCoord.DMSs:
                    ((ControlStation)AbstractPropertyGrid).Local.LocationInfo.ViewCoordField = OptimaControlStationProperties.Model.ViewCoord.DMSs;
                    break;
            }
        }
    }
}
