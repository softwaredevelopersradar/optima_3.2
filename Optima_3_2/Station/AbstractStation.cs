﻿using Mapsui.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.WpfPropertyGrid;
using WpfMapControl;

namespace Optima_3_2
{
    public abstract class AbstractStation
    {
        public IMapObject StaitionImage { get; set; }
        public IFeature StationPolyLine { get; set; }

        public abstract double Latitude { get; set; }
       
        public abstract double Longitude { get; set; }

        public abstract string Note { get; set; }


        public UserControl AbstractPropertyGrid { get; set; }


        public abstract void SetLanguage(OptimaSettingsProperties.Model.Languages languages);
        public abstract void SetCoordView(OptimaSettingsProperties.Model.ViewCoord viewCoord);


        public void ChangePropertyGridVisibility(bool isVisible)
        {
            if (isVisible)
                AbstractPropertyGrid.Visibility = Visibility.Visible;
            else
                AbstractPropertyGrid.Visibility = Visibility.Collapsed;
        }

        protected string CutRightZerosOfString(string str)
        {
            if (str.Contains(',') == true)
                str.TrimEnd('0');
            return str;
        }

    }
}
