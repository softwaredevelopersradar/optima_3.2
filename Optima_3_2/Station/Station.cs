﻿using CoordFormatLib;
using GeoUtility.GeoSystem;
using Mapsui.Providers;
using ModemControl;
using OptimaControlStationProperties.Model;
using OptimaStationsProperties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using WpfMapControl;

namespace Optima_3_2
{
    public class Station : AbstractStation
    {
        public IFeature StationLine { get; set; }

        public event EventHandler<OptimaStationsProperties.Model.PropertiesOfMapObject> OnApplyButtonClick = (sender, obj) => { };
        public event EventHandler<OptimaStationsProperties.Model.PropertiesOfMapObject> OnDeleteButtonClick = (sender, obj) => { };

        public delegate void UpdateHandler(int j, ModemModel e);
        public event UpdateHandler OnUpdateModemModel = (j, modemModelItem) => { };

        private ModemModel modemModelItem;

        public bool InitializationFlag { get; set; } = false;
        public int Num { get; set; }

        public byte NumVi
        {
            get
            {
                return ((MapObjectPropGrid)AbstractPropertyGrid).Local.Common.Num;
            }
            set
            {
                ((MapObjectPropGrid)AbstractPropertyGrid).Local.Common.Num = value;
                modemModelItem.Number = ((MapObjectPropGrid)AbstractPropertyGrid).Local.Common.Num;
                OnUpdateModemModel(Num, modemModelItem);
            }
        }
        public override double Latitude
        {
            get
            {
                return ((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.Latitude;
            }
            set
            {
                ((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.Latitude = value;
                modemModelItem.LatitudeStr = ((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.LatitudeStr;
                OnUpdateModemModel(Num, modemModelItem);
            }
        }

        public override double Longitude
        {
            get
            {
                return ((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.Longitude;
            }
            set
            {
                ((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.Longitude = value;
                modemModelItem.LongitudeStr = ((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.LongitudeStr;
                OnUpdateModemModel(Num, modemModelItem);
            }
        }

        public double Distance
        {
            get
            {
                return ((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.Distance;
            }
            set
            {
                ((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.Distance = value;
            }
        }

        public double Direction
        {
            get
            {
                return ((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.Direction;
            }
            set
            {
                ((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.Direction = value;
            }
        }

        public string IpAddress
        {
            get
            {
                return ((MapObjectPropGrid)AbstractPropertyGrid).Local.Connection.IpAddress;
            }
            set
            {
                ((MapObjectPropGrid)AbstractPropertyGrid).Local.Connection.IpAddress = value;
                modemModelItem.IPAddress = ((MapObjectPropGrid)AbstractPropertyGrid).Local.Connection.IpAddress;
                OnUpdateModemModel(Num, modemModelItem);
            }
        }

        public int Port
        {
            get
            {
                return ((MapObjectPropGrid)AbstractPropertyGrid).Local.Connection.Port;
            }
            set
            {
                ((MapObjectPropGrid)AbstractPropertyGrid).Local.Connection.Port = value;   
            }
        }

        public override string Note
        {
            get
            {
                return ((MapObjectPropGrid)AbstractPropertyGrid).Local.Common.Note;
            }
            set
            {
                ((MapObjectPropGrid)AbstractPropertyGrid).Local.Common.Note = value;
                modemModelItem.Note = ((MapObjectPropGrid)AbstractPropertyGrid).Local.Common.Note;
                OnUpdateModemModel(Num, modemModelItem);
            }
        }

        public GLONASS GLONASS
        {
            get
            {
                return modemModelItem.GLONASS;
            }
            set {
                modemModelItem.GLONASS = value;
                OnUpdateModemModel(Num, modemModelItem);
            }
        }
        public GPS GPS
        {
            get
            {
                return modemModelItem.GPS;
            }
            set
            {
                modemModelItem.GPS = value;
                OnUpdateModemModel(Num, modemModelItem);
            }
        }

        public Galileo Galileo
        {
            get
            {
                return modemModelItem.Galileo;
            }
            set
            {
                modemModelItem.Galileo = value;
                OnUpdateModemModel(Num, modemModelItem);
            }
        }

        public Beidou Beidou
        {
            get
            {
                return modemModelItem.Beidou;
            }
            set
            {
                modemModelItem.Beidou = value;
                OnUpdateModemModel(Num, modemModelItem);
            }
        }

        public Radio RadioF
        {
            get
            {
                return modemModelItem.RadioF;
            }
            set
            {
                modemModelItem.RadioF = value;
                OnUpdateModemModel(Num, modemModelItem);
            }
        }

        public Led Radio
        {
            get
            {
                return modemModelItem.Radio;
            }
            set
            {
                modemModelItem.Radio = value;
                OnUpdateModemModel(Num, modemModelItem);
            }
        }

        public Led GSM
        {
            get
            {
                return modemModelItem.GSM;
            }
            set
            {
                modemModelItem.GSM = value;
                OnUpdateModemModel(Num, modemModelItem);
            }
        }

        public Led RM
        {
            get
            {
                return modemModelItem.RM;
            }
            set
            {
                modemModelItem.RM = value;
                OnUpdateModemModel(Num, modemModelItem);
            }
        }

        public byte FreqPC
        {
            get
            {
                return modemModelItem.FreqPC;
            }
            set
            {
                modemModelItem.FreqPC = value;
                OnUpdateModemModel(Num, modemModelItem);
            }
        }

        public Station() { }
        public Station(Location location, OptimaSettingsProperties.Model.Languages languages, OptimaSettingsProperties.Model.ViewCoord viewCoord, int num)
        {
            AbstractPropertyGrid = new MapObjectPropGrid();
            CreateMapObjectPropertyGrid(location.Latitude, location.Longitude, viewCoord, num);
            Num = num;
            modemModelItem = new ModemModel();
            CreateModemModel();

            SetLanguage(languages);
            SetCoordView(viewCoord);
            ((MapObjectPropGrid)AbstractPropertyGrid).OnApplyButtonClick += MapObjectPropGrid_OnApplyButtonClick;
            ((MapObjectPropGrid)AbstractPropertyGrid).OnDeleteButtonClick += MapObjectPropGrid_OnDeleteButtonClick;
        }

        private void MapObjectPropGrid_OnDeleteButtonClick(object sender, OptimaStationsProperties.Model.PropertiesOfMapObject e)
        {
            OnDeleteButtonClick(sender, e);
        }

        private void MapObjectPropGrid_OnApplyButtonClick(object sender, OptimaStationsProperties.Model.PropertiesOfMapObject e)
        {
            OnApplyButtonClick(sender, e);
        }

        private void CreateMapObjectPropertyGrid(double latitude, double longitude, OptimaSettingsProperties.Model.ViewCoord viewCoord, int i)
        {
            ChangeCoordinateViewForStationsPropGrid(viewCoord);
            ((MapObjectPropGrid)AbstractPropertyGrid).Local.Common.Num = Convert.ToByte(i);
            ((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.Latitude = Math.Round(latitude, 4);
            ((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.Longitude = Math.Round(longitude, 4);
            PutStationPropGridOnWindow();
        }

        private void PutStationPropGridOnWindow()
        {
            Grid.SetRow(AbstractPropertyGrid, 1);
            Grid.SetZIndex(AbstractPropertyGrid, 1);
            Grid.SetColumn(AbstractPropertyGrid, 0);
        }

        public override void SetLanguage(OptimaSettingsProperties.Model.Languages languages)
        {
            ((MapObjectPropGrid)AbstractPropertyGrid).Local.Common.Language = (OptimaStationsProperties.Model.Languages)languages;
        }

        public override void SetCoordView(OptimaSettingsProperties.Model.ViewCoord viewCoord)
        {
            ChangeCoordinateViewForStationsPropGrid(viewCoord);
            TranslateCoordinate();
            modemModelItem.LatitudeStr = ((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.LatitudeStr;
            modemModelItem.LongitudeStr = ((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.LongitudeStr;
            OnUpdateModemModel(Num, modemModelItem);
        }

        public void AllLedColor(Led color)
        {
            modemModelItem.GLONASS.L1 = color;
            modemModelItem.GLONASS.L2 = color;
            modemModelItem.GPS.L1 = color;
            modemModelItem.GPS.L2 = color;
            modemModelItem.GSM = color;
            modemModelItem.RadioF.F1 = color;
            modemModelItem.RadioF.F2 = color;
            modemModelItem.Radio = color;
            modemModelItem.RM = color;
            modemModelItem.Beidou.L1 = color;
            modemModelItem.Beidou.L2 = color;
            modemModelItem.Galileo.L1 = color;
            modemModelItem.Galileo.L2 = color;

            OnUpdateModemModel(Num, modemModelItem);
        }

        public void UpdateModemModel()
        {
            OnUpdateModemModel(Num, modemModelItem);
        }

        public void LedsColor(bool GOLONAS_L1, bool GOLONAS_L2, bool GPS_L1, bool GPS_L2, bool GSM, bool RadioF_F1, bool RadioF_F2, bool Galileo_L1, bool Galileo_L2, bool Beidou_L1, bool Beidou_L2, bool Radio, bool RM, bool Existance_L1, bool Existance_L2)
        {
            Led color_L1;
            Led color_L2;

            if (Existance_L1 == true)
                color_L1 = Led.Green;
            else color_L1 = Led.Yellow;

            if (Existance_L2 == true)
                color_L2 = Led.Green;
            else color_L2 = Led.Yellow;

            

            modemModelItem.GLONASS.L1 = (GOLONAS_L1 == true ? color_L1 : Led.Red);
            modemModelItem.GLONASS.L2 = (GOLONAS_L2 == true ? color_L2 : Led.Red);
            modemModelItem.GPS.L1 = (GPS_L1 == true ? color_L1 : Led.Red);
            modemModelItem.GPS.L2 = (GPS_L2 == true ? color_L2 : Led.Red);
            modemModelItem.Galileo.L1 = (Galileo_L1 == true ? color_L1 : Led.Red);
            modemModelItem.Galileo.L2 = (Galileo_L2 == true ? color_L2 : Led.Red);
            modemModelItem.Beidou.L1 = (Beidou_L1 == true ? color_L1 : Led.Red);
            modemModelItem.Beidou.L2 = (Beidou_L2 == true ? color_L2 : Led.Red);
            modemModelItem.GSM = (GSM == true ? Led.Green : Led.Red);
            modemModelItem.RadioF.F1 = (RadioF_F1 == true ? Led.Green : Led.Red);
            modemModelItem.RadioF.F2 = (RadioF_F2 == true ? Led.Green : Led.Red);
            modemModelItem.Radio = (Radio == true ? Led.Green : Led.Red);
            modemModelItem.RM = (RM == true ? Led.Green : Led.Red);

            OnUpdateModemModel(Num, modemModelItem);
        }

        public void CreateModemModel()
        {
            modemModelItem.IPAddress = ((MapObjectPropGrid)AbstractPropertyGrid).Local.Connection.IpAddress;
            modemModelItem.Number = ((MapObjectPropGrid)AbstractPropertyGrid).Local.Common.Num;
            modemModelItem.Note = ((MapObjectPropGrid)AbstractPropertyGrid).Local.Common.Note;
            modemModelItem.LatitudeStr = ((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.LatitudeStr;
            modemModelItem.LongitudeStr = ((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.LongitudeStr;
            modemModelItem.GLONASS = new GLONASS();
            modemModelItem.GPS = new GPS();
            modemModelItem.Galileo = new Galileo();
            modemModelItem.Beidou = new Beidou();
            modemModelItem.RadioF = new Radio();
            AllLedColor(Led.Gray);
        }


        private void ChangeCoordinateViewForStationsPropGrid(OptimaSettingsProperties.Model.ViewCoord viewCoord)
        {
            switch (viewCoord)
            {
                case OptimaSettingsProperties.Model.ViewCoord.Dd:
                    ((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.ViewCoordField = OptimaStationsProperties.Model.ViewCoord.Dd;
                    break;
                case OptimaSettingsProperties.Model.ViewCoord.DMm:
                    ((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.ViewCoordField = OptimaStationsProperties.Model.ViewCoord.DMm;
                    break;
                case OptimaSettingsProperties.Model.ViewCoord.DMSs:
                    ((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.ViewCoordField = OptimaStationsProperties.Model.ViewCoord.DMSs;
                    break;
            }
        }

        public void TranslateCoordinate()
        {

            VCFormat VCoordSpooffing = new VCFormat(((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.Latitude, ((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.Longitude);

            switch (((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.ViewCoordField)
            {
                case OptimaStationsProperties.Model.ViewCoord.DMm:

                    if (Math.Round(Math.Abs(VCoordSpooffing.coordDegMin.Latitude.Minute), 4) >= 60)
                        ((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.LatitudeStr = (VCoordSpooffing.coordDegMin.Latitude.Degree + 1).ToString() + "°" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSpooffing.coordDegMin.Latitude.Minute) - 60, 4).ToString()) + "'";
                    else ((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.LatitudeStr = (VCoordSpooffing.coordDegMin.Latitude.Degree).ToString() + "°" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSpooffing.coordDegMin.Latitude.Minute), 4).ToString()) + "'";

                    if (Math.Round(Math.Abs(VCoordSpooffing.coordDegMin.Longitude.Minute), 4) >= 60)
                        ((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.LongitudeStr = (VCoordSpooffing.coordDegMin.Longitude.Degree + 1).ToString() + "°" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSpooffing.coordDegMin.Longitude.Minute) - 60, 4).ToString()) + "'";
                    else ((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.LongitudeStr = (VCoordSpooffing.coordDegMin.Longitude.Degree).ToString() + "°" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSpooffing.coordDegMin.Longitude.Minute), 4).ToString()) + "'";

                    break;
                case OptimaStationsProperties.Model.ViewCoord.DMSs:
                    if (Math.Round(Math.Abs(VCoordSpooffing.coordDegMinSec.Latitude.Second)) >= 60)
                        ((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.LatitudeStr = (VCoordSpooffing.coordDegMinSec.Latitude.Degree).ToString() + "°" + (Math.Abs(VCoordSpooffing.coordDegMinSec.Latitude.Minute) + 1).ToString() + "'" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSpooffing.coordDegMinSec.Latitude.Second) - 60).ToString()) + "\"";
                    else ((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.LatitudeStr = (VCoordSpooffing.coordDegMinSec.Latitude.Degree).ToString() + "°" + (Math.Abs(VCoordSpooffing.coordDegMinSec.Latitude.Minute)).ToString() + "'" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSpooffing.coordDegMinSec.Latitude.Second)).ToString()) + "\"";

                    if (Math.Round(Math.Abs(VCoordSpooffing.coordDegMinSec.Longitude.Second)) >= 60)
                        ((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.LongitudeStr = (VCoordSpooffing.coordDegMinSec.Longitude.Degree).ToString() + "°" + (Math.Abs(VCoordSpooffing.coordDegMinSec.Longitude.Minute) + 1).ToString() + "'" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSpooffing.coordDegMinSec.Longitude.Second) - 60).ToString()) + "\"";
                    else ((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.LongitudeStr = (VCoordSpooffing.coordDegMinSec.Longitude.Degree).ToString() + "°" + (Math.Abs(VCoordSpooffing.coordDegMinSec.Longitude.Minute)).ToString() + "'" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSpooffing.coordDegMinSec.Longitude.Second)).ToString()) + "\"";

                    break;
                case OptimaStationsProperties.Model.ViewCoord.Dd:
                    ((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.LatitudeStr = CutRightZerosOfString(Math.Round(VCoordSpooffing.coordDeg.Latitude.Degree, 6).ToString()) + "°";
                    ((MapObjectPropGrid)AbstractPropertyGrid).Local.LocationInfo.LongitudeStr = CutRightZerosOfString(Math.Round(VCoordSpooffing.coordDeg.Longitude.Degree, 6).ToString()) + "°";

                    break;
                default: break;
            }
        }

        public Mapsui.Geometries.Point[] DrawRange()
        {
            Location coord = new Location(Longitude, Latitude);
            UTM uTM = new UTM();
            string errorstr = "";
            Dictionary<string, bool> keyValuePairs = new Dictionary<string, bool>();
            string[] str = coord.ToUtm().Split(new char[] { ' ' });
            UTM.TryParse(str[0].Substring(0, 2), str[0].Substring(2, 1), (str[str.Length - 3].Split(new char[] { ',', '.' }))[0], (str[str.Length - 1].Split(new char[] { ',', '.' }))[0], out uTM, out errorstr, out keyValuePairs);

            double cosalph = Math.Cos((Math.PI / 180) * Direction);
            double sinalph = Math.Sin((Math.PI / 180) * Direction);
            var coordOfRange11 = new[] {
              new UTM(uTM.Zone, uTM.Band, uTM.East + 2*(Distance/5) * cosalph, uTM.North - 2*(Distance/5) * sinalph),
              new UTM(uTM.Zone, uTM.Band, uTM.East + (0.95*(Distance/2) * cosalph + (Distance/5) * sinalph),  uTM.North + (Distance/5) * cosalph - 0.95*(Distance/2) * sinalph),
              new UTM(uTM.Zone, uTM.Band, uTM.East + (0.98*(Distance/2) * cosalph + 2*(Distance/5) * sinalph), uTM.North + 2*(Distance/5) * cosalph - 0.98*(Distance/2) * sinalph),
              new UTM(uTM.Zone, uTM.Band, uTM.East + (2.2*(Distance/5) * cosalph + 3*(Distance/5) * sinalph), uTM.North + 3*(Distance/5) * cosalph - 2.2*(Distance/5) * sinalph),
              new UTM(uTM.Zone, uTM.Band, uTM.East + (1.65*(Distance/5) * cosalph + 4*(Distance/5) * sinalph), uTM.North + 4*(Distance/5) * cosalph - 1.65*(Distance/5) * sinalph),
              new UTM(uTM.Zone, uTM.Band, uTM.East + (0.9*(Distance/5) * cosalph + 4.7*(Distance/5) * sinalph), uTM.North + 4.7*(Distance/5) * cosalph - 0.9*(Distance/5) * sinalph),
              new UTM(uTM.Zone, uTM.Band, uTM.East + Distance * sinalph, uTM.North + Distance * cosalph),
              new UTM(uTM.Zone, uTM.Band, uTM.East - (0.9*(Distance/5) * cosalph - 4.7*(Distance/5) * sinalph), uTM.North + 4.7*(Distance/5) * cosalph + 0.9*(Distance/5) * sinalph),
              new UTM(uTM.Zone, uTM.Band, uTM.East - (1.65*(Distance/5) * cosalph - 4*(Distance/5) * sinalph), uTM.North + 4*(Distance/5) * cosalph + 1.65*(Distance/5) * sinalph),
              new UTM(uTM.Zone, uTM.Band, uTM.East - (2.2*(Distance/5) * cosalph - 3*(Distance/5) * sinalph), uTM.North + 3*(Distance/5) * cosalph + 2.2*(Distance/5) * sinalph),
              new UTM(uTM.Zone, uTM.Band, uTM.East - (0.98*(Distance/2) * cosalph - 2*(Distance/5) * sinalph), uTM.North + 2*(Distance/5) * cosalph + 0.98*(Distance/2) * sinalph),
              new UTM(uTM.Zone, uTM.Band, uTM.East - (0.95*(Distance/2) * cosalph - (Distance/5) * sinalph),  uTM.North + (Distance/5) * cosalph + 0.95*(Distance/2) * sinalph),
              new UTM(uTM.Zone, uTM.Band, uTM.East - 2*(Distance/5) * cosalph, uTM.North + 2*(Distance/5) * sinalph),
              new UTM(uTM.Zone, uTM.Band, uTM.East - ((Distance/5) * cosalph + (Distance/5) * sinalph), uTM.North - ((Distance/5) * cosalph - (Distance/5) * sinalph)),
              new UTM(uTM.Zone, uTM.Band, uTM.East + ((Distance/5) * cosalph - (Distance/5) * sinalph), uTM.North - ((Distance/5) * cosalph + (Distance/5) * sinalph)),
            };
            uint len = Convert.ToUInt32(coordOfRange11.Length);
            var point = new Mapsui.Geometries.Point[len];
            for (int j = 0; j < coordOfRange11.Length; j++)
                point[j] = Mapsui.Projection.Mercator.FromLonLat(((Geographic)coordOfRange11[j]).Longitude, ((Geographic)coordOfRange11[j]).Latitude);

            return point;
        }

        public Mapsui.Geometries.Point[] DrawLines(Mapsui.Geometries.Point ControlStationPoint)
        {
            Location coord = new Location(Longitude, Latitude);
            Location coordMainStation = new Location(ControlStationPoint.X, ControlStationPoint.Y);

            UTM uTM = new UTM();
            UTM uTM2 = new UTM();
            string errorstr = "";
            Dictionary<string, bool> keyValuePairs = new Dictionary<string, bool>();
            string[] str = coord.ToUtm().Split(new char[] { ' ' });
            UTM.TryParse(str[0].Substring(0, 2), str[0].Substring(2, 1), (str[str.Length - 3].Split(new char[] { ',', '.' }))[0], (str[str.Length - 1].Split(new char[] { ',', '.' }))[0], out uTM, out errorstr, out keyValuePairs);

            string[] str2 = coordMainStation.ToUtm().Split(new char[] { ' ' });
            UTM.TryParse(str2[0].Substring(0, 2), str2[0].Substring(2, 1), (str2[str.Length - 3].Split(new char[] { ',', '.' }))[0], (str2[str.Length - 1].Split(new char[] { ',', '.' }))[0], out uTM2, out errorstr, out keyValuePairs);

            var coordOfRange11 = new[] {
              new UTM(uTM.Zone, uTM.Band, uTM.East, uTM.North),
              new UTM(uTM2.Zone, uTM2.Band, uTM2.East,  uTM2.North),              
            };
            uint len = Convert.ToUInt32(coordOfRange11.Length);
            var point = new Mapsui.Geometries.Point[len];
            for (int j = 0; j < coordOfRange11.Length; j++)
                point[j] = Mapsui.Projection.Mercator.FromLonLat(((Geographic)coordOfRange11[j]).Longitude, ((Geographic)coordOfRange11[j]).Latitude);

            return point;
        }

    }
}
