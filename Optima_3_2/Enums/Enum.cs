﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optima_3_2.Enums
{
   public enum StationStatus
    {
        Normal,
        WithoutRadiation,
        Error
    }

   public enum CommandType
    {
        ON,
        OFF,
        POLL
    }
}
