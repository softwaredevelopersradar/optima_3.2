﻿
using BRD;
using BRD.Events;
using GrozaSModelsDBLib;
using System;
using System.Threading;
using System.Windows;


namespace Optima_3_2
{
    public partial class MainWindow : Window
    {

        private void BRDControlConnection_ButServerClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (comBRD != null)
                    DisconnectBRD();
                else
                {
                    ConnectBRD();
                }
            }
            catch
            { }
        }
        private void OpenPortBRD(object sender, EventArgs e)
        {
            
        }

        private void ClosePortBRD(object sender, EventArgs e)
        {
           
        }

        private void EchoBRD(object sender, ErrorEventArgs e)
        { }

        private void FreeRotateBRD(object sender, ErrorEventArgs e)
        { }

        private void GetAngleBRD(object sender, AngleEventArgs e)
        {
            
        }

        private void SetAngleBRD(object sender, BRD.Events.ErrorEventArgs e)
        { }

        private void StopBRD(object sender, ErrorEventArgs e)
        { }

        private void HandlerJammerPositionEvent(object sender, WpfMapControl.Location e)
        {
           
        }

        private void SetAngleBRD(float e)
        {
            try
            {


            }
            catch { }
        }


    }
}