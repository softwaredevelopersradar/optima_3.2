﻿using BRD;
using System.Windows;


namespace Optima_3_2
{
    public partial class MainWindow : Window
    {
        public ComBRD comBRD;

        private void ConnectBRD()
        {
            if (comBRD != null)
                DisconnectBRD();

            comBRD = new ComBRD((byte)propertiesPropGrid.Local.BRD.Address);

          
            comBRD.OnOpenPort += OpenPortBRD;
            comBRD.OnClosePort += ClosePortBRD;          
            comBRD.OnEcho += EchoBRD;
            comBRD.OnFreeRotate += FreeRotateBRD;
            comBRD.OnGetAngle += GetAngleBRD;
            comBRD.OnSetAngle += SetAngleBRD;
            comBRD.OnStop += StopBRD;

            comBRD.OpenPort(propertiesPropGrid.Local.BRD.ComPort, 19200, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);

            comBRD.Calibration();
        }

        private void DisconnectBRD()
        {
            if (comBRD != null)
            {
                comBRD.ClosePort();

                comBRD.OnOpenPort -= OpenPortBRD;
                comBRD.OnClosePort -= ClosePortBRD;
                comBRD.OnEcho -= EchoBRD;
                comBRD.OnFreeRotate -= FreeRotateBRD;
                comBRD.OnGetAngle -= GetAngleBRD;
                comBRD.OnSetAngle -= SetAngleBRD;
                comBRD.OnStop -= StopBRD;

                comBRD = null;
            }

        }

    }
}
