﻿using OptimaControlStationProperties.Ext;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.WpfPropertyGrid;

namespace OptimaControlStationProperties.Model
{
    public class Common : AbstractBaseProperties<Common>
    {
        private string name = "";
        private String note = " ";
        private Languages language;

        [NotifyParentProperty(true)]
        public string Name
        {
            get => name;
            set
            {
                if (name == value) return;
                name = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public String Note
        {
            get => note;
            set
            {
                if (note == value) return;
                note = value;
                OnPropertyChanged();
            }
        }

        public Languages Language
        {
            get => language;
            set
            {
                if (language == value) return;
                language = value;
                OnPropertyChanged();
            }
        }

        public override Common Clone()
        {
            return new Common
            {
                Name = Name,
                Note = Note,
                Language = Language,
            };
        }

        public override bool EqualTo(Common model)
        {
            return Name == model.Name
                && Note == model.Note
                && Language == model.Language;              
        }

        public override void Update(Common model)
        {
            Name = model.Name;
            Note = model.Note;
            Language = model.Language;
            
        }
    }
}
