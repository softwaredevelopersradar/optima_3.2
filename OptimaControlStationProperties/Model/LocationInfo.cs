﻿using CoordFormatLib;
using OptimaControlStationProperties.Ext;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.WpfPropertyGrid;
using YamlDotNet.Serialization;

namespace OptimaControlStationProperties.Model
{
    public class LocationInfo : AbstractBaseProperties<LocationInfo>
    {
        private ViewCoord viewCoord;
        private double _latitude = -1;
        private double _longitude = -1;

        public LocationInfo()
        {
            mass = new char[] { '°', '\'', '"' };
        }


        #region UnvisibleProperties

        [YamlIgnore]
        [Browsable(false)]
        public ViewCoord ViewCoordField
        {
            get { return viewCoord; }
            set
            {
                if (viewCoord == value) return;
                viewCoord = value;
                OnPropertyChanged();
            }
        }


        [YamlIgnore]
        [Range(-90, 90)]
        [Browsable(false)]
        public double Latitude
        {
            get { return _latitude; }
            set
            {
                if (_latitude == value) return;
                _latitude = value;
                ConvertDoubleToCoord();
                OnPropertyChanged();
            }
        }


        [YamlIgnore]
        [Range(-180, 180)]
        [Browsable(false)]
        public double Longitude
        {
            get { return _longitude; }
            set
            {
                if (_longitude == value) return;
                _longitude = value;
                ConvertDoubleToCoord();
                OnPropertyChanged();
            }
        }

        #endregion

        #region VisibleProperties

        private string str1 = "";
        private string str2 = "";
        private char[] mass;

        [NotifyParentProperty(true)]
        [RegularExpression("^-{0,1}((90|[0]?[0-9]{1}|[1-8]{1}[0-9]{1})($|[°]{1}$|([.|,]{1}($|[0-9]{1,6}($|[°]{1}$)))|[°]{1}(60$|60[\"|']{1}$|([0]?[0-9]{1}|[0-5]{1}[0-9]{1})($|[\"|']{1}$|[.|,]{1}[0-9]{1,6}($|[\"|']{1}$)|([\"|']{1}[0-9]$|[\"|']{1}(60$|60[\"|']{1}$|[0]?[0-9]{1}|[0-5]{1}[0-9]{1})($|[.|,]{1}[0-9]{1,6}($|[\"|']{1}$)|[\"|']{1}$))))))", ErrorMessage = "XXX°XX'XX.XXX\"")]
        public string LatitudeStr
        {
            get { return str1; }
            set
            {

                if (str1 == value)
                    return;
                else
                    str1 = value;

                if (str1.IndexOf("-") > -1)
                    _latitude = ConvertCoordToDouble(-1, str1);
                else
                    _latitude = ConvertCoordToDouble(1, str1);
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [RegularExpression("^-{0,1}((180|[0]{0,2}[0-9]{1}|[0]?[0-9]{2}|[1]{1}[0-7]{1}[0-9]{1})($|[°]{1}$|([.|,]{1}($|[0-9]{1,6}($|[°]{1}$)))|[°]{1}(60$|60[\"|']{1}$|([0]?[0-9]{1}|[0-5]{1}[0-9]{1})($|[\"|']{1}$|[.|,]{1}[0-9]{1,6}($|[\"|']{1}$)|([\"|']{1}[0-9]$|[\"|']{1}(60$|60[\"|']{1}$|[0]?[0-9]{1}|[0-5]{1}[0-9]{1})($|[.|,]{1}[0-9]{1,6}($|[\"|']{1}$)|[\"|']{1}$))))))", ErrorMessage = "XXX°XX'XX.XXX\"")]
        public string LongitudeStr
        {
            get { return str2; }
            set
            {
                if (str2 == value) return;
                else
                    str2 = value;

                if (str2.IndexOf("-") > -1)
                    _longitude = ConvertCoordToDouble(-1, str2);
                else
                    _longitude = ConvertCoordToDouble(1, str2);
                OnPropertyChanged();
            }
        }
        #endregion

        #region ConvertCoordFunction
        private double ConvertCoordToDouble(int PositiveOrNegativ, string str)
        {
            double coord = 0;
            try
            {
                string[] numbers = str.Split(mass);
                if (numbers.Length <= 1)
                    coord = Convert.ToDouble(str);
                else if (numbers.Length == 2 && numbers[1] == "")
                    coord = Convert.ToDouble(str.Trim(mass));
                else if ((numbers.Length == 3 && numbers[2] == "") || (numbers.Length == 2 && numbers[1] != ""))
                    coord = Convert.ToDouble(numbers[0].Trim(mass)) + PositiveOrNegativ * Convert.ToDouble(numbers[1].Trim(mass)) * (1.0 / 60.0);
                else if (numbers.Length == 4 || (numbers.Length == 3 && numbers[2] != ""))
                    coord = Convert.ToDouble(numbers[0].Trim(mass)) + PositiveOrNegativ * Convert.ToDouble(numbers[1].Trim(mass)) * (1.0 / 60.0) + PositiveOrNegativ * Convert.ToDouble(numbers[2].Trim(mass)) * (1.0 / 3600.0);
            }
            catch (Exception)
            {
            }

            return coord;
        }
        private string CutRightZerosOfString(string str)
        {
            if (str.Contains(",") == true)
                str.TrimEnd('0');
            return str;
        }

        private void ConvertDoubleToCoord()
        {
            VCFormat vcCoord = new VCFormat(_latitude, _longitude);

            switch (viewCoord)
            {
                case ViewCoord.DMm:
                    if (Math.Round(Math.Abs(vcCoord.coordDegMin.Latitude.Minute), 4) >= 60)
                        LatitudeStr = (vcCoord.coordDegMin.Latitude.Degree + 1).ToString() + "°" + CutRightZerosOfString(Math.Round(Math.Abs(vcCoord.coordDegMin.Latitude.Minute) - 60, 4).ToString()) + "'";
                    else LatitudeStr = (vcCoord.coordDegMin.Latitude.Degree).ToString() + "°" + CutRightZerosOfString(Math.Round(Math.Abs(vcCoord.coordDegMin.Latitude.Minute), 4).ToString()) + "'";
                    if (Math.Round(Math.Abs(vcCoord.coordDegMin.Longitude.Minute), 4) >= 60)
                        LongitudeStr = (vcCoord.coordDegMin.Longitude.Degree + 1).ToString() + "°" + CutRightZerosOfString(Math.Round(Math.Abs(vcCoord.coordDegMin.Longitude.Minute) - 60, 4).ToString()) + "'";
                    else LongitudeStr = (vcCoord.coordDegMin.Longitude.Degree).ToString() + "°" + CutRightZerosOfString(Math.Round(Math.Abs(vcCoord.coordDegMin.Longitude.Minute), 4).ToString()) + "'";
                    break;

                case ViewCoord.DMSs:
                    if (Math.Round(Math.Abs(vcCoord.coordDegMinSec.Latitude.Second)) >= 60)
                        LatitudeStr = (vcCoord.coordDegMinSec.Latitude.Degree).ToString() + "°" + (Math.Abs(vcCoord.coordDegMinSec.Latitude.Minute) + 1).ToString() + "'" + CutRightZerosOfString(Math.Round(Math.Abs(vcCoord.coordDegMinSec.Latitude.Second) - 60).ToString()) + "\"";
                    else LatitudeStr = (vcCoord.coordDegMinSec.Latitude.Degree).ToString() + "°" + (Math.Abs(vcCoord.coordDegMinSec.Latitude.Minute)).ToString() + "'" + CutRightZerosOfString(Math.Round(Math.Abs(vcCoord.coordDegMinSec.Latitude.Second)).ToString()) + "\"";
                    if (Math.Round(Math.Abs(vcCoord.coordDegMinSec.Longitude.Second)) >= 60)
                        LongitudeStr = (vcCoord.coordDegMinSec.Longitude.Degree).ToString() + "°" + (Math.Abs(vcCoord.coordDegMinSec.Longitude.Minute) + 1).ToString() + "'" + CutRightZerosOfString(Math.Round(Math.Abs(vcCoord.coordDegMinSec.Longitude.Second) - 60).ToString()) + "\"";
                    else LongitudeStr = (vcCoord.coordDegMinSec.Longitude.Degree).ToString() + "°" + (Math.Abs(vcCoord.coordDegMinSec.Longitude.Minute)).ToString() + "'" + CutRightZerosOfString(Math.Round(Math.Abs(vcCoord.coordDegMinSec.Longitude.Second)).ToString()) + "\"";
                    break;

                case ViewCoord.Dd:
                    LatitudeStr = CutRightZerosOfString(Math.Round(vcCoord.coordDeg.Latitude.Degree, 6).ToString()) + "°";
                    LongitudeStr = CutRightZerosOfString(Math.Round(vcCoord.coordDeg.Longitude.Degree, 6).ToString()) + "°";
                    break;

                default: break;
            }

        }
        #endregion


        public override LocationInfo Clone()
        {
            return new LocationInfo
            {
                Latitude = Latitude,
                Longitude = Longitude,
                LatitudeStr = LatitudeStr,
                LongitudeStr = LongitudeStr,
                ViewCoordField = ViewCoordField
            };
        }

        public override bool EqualTo(LocationInfo model)
        {
            return Latitude == model.Latitude
                && Longitude == model.Longitude
                && LatitudeStr == model.LatitudeStr
                && LongitudeStr == model.LongitudeStr
                && ViewCoordField == model.ViewCoordField;
        }

        public override void Update(LocationInfo model)
        {
            Latitude = model.Latitude;
            Longitude = model.Longitude;
            LatitudeStr = model.LatitudeStr;
            LongitudeStr = model.LongitudeStr;
            ViewCoordField = model.ViewCoordField;
        }
    }
}
