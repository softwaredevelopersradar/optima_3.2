﻿using OptimaControlStationProperties.Ext;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.WpfPropertyGrid;

namespace OptimaControlStationProperties.Model
{
    [CategoryOrder(nameof(Common), 1)]
    [CategoryOrder(nameof(LocationInfo), 2)]
    public class ControlStationProperties : IModelMethods<ControlStationProperties>
    {
        public ControlStationProperties()
        {
            Common = new Common();
            LocationInfo = new LocationInfo();

            Common.PropertyChanged += PropertyChanged;
            LocationInfo.PropertyChanged += PropertyChanged;
        }

        public event PropertyChangedEventHandler OnPropertyChanged = (obg, str) => { };

        private void PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(this, e);
        }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(Common))]
        [DisplayName(" ")]
        public Common Common { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(LocationInfo))]
        [DisplayName(" ")]
        public LocationInfo LocationInfo { get; set; }


        public ControlStationProperties Clone()
        {
            return new ControlStationProperties
            {
                Common = Common.Clone(),
                LocationInfo = LocationInfo.Clone(),
            };
        }

        public  bool EqualTo(ControlStationProperties model)
        {
            return Common.EqualTo(model.Common)
                  && LocationInfo.EqualTo(model.LocationInfo);
        }

        public  void Update(ControlStationProperties model)
        {
            Common.Update(model.Common);
            LocationInfo.Update(model.LocationInfo);
        }
    }
}
