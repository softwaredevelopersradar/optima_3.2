﻿using OptimaControlStationProperties.Model;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;


namespace OptimaControlStationProperties.Editors
{
    public class EditorCustom : PropertyEditor
    {
        Dictionary<Type, string> dictKeyDataTemplate = new Dictionary<Type, string>()
        {
            { typeof(Common), "CommonEditorKey"},
            { typeof(LocationInfo), "LocationInfoEditorKey"},
        };

        public EditorCustom(string PropertyName, Type DeclaringType, Type typeProperty)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/OptimaControlStationProperties;component/Editors/Templates.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            this.InlineTemplate = resource[dictKeyDataTemplate[typeProperty]];
        }

        public EditorCustom(string PropertyName, Type DeclaringType, string nameEditorKey)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/OptimaControlStationProperties;component/Editors/Templates.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            this.InlineTemplate = resource[nameEditorKey];
        }
    }
}
