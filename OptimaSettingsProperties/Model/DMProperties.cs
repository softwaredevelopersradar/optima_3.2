﻿using OptimaSettingsProperties.Ext;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.WpfPropertyGrid;

namespace OptimaSettingsProperties.Model
{
   public class DMProperties : AbstractBaseProperties<DMProperties>
    {

        private string ipAddress = "127.0.0.1";
        private int port = 88;
        private bool existance = false;
        private int correctionAngle = 1;

        public DMProperties() { }

        [NotifyParentProperty(true)]
        public bool Availability
        {
            get => existance;
            set
            {
                if (existance == value) return;
                existance = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IpAddress
        {
            get => ipAddress;
            set
            {
                if (ipAddress == value) return;
                ipAddress = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Range(1, 1000000)]
        public int Port
        {
            get => port;
            set
            {
                if (port == value) return;
                port = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Range(1, 1000000)]
        public int CorrectionAngle
        {
            get => correctionAngle;
            set
            {
                if (correctionAngle == value) return;
                correctionAngle = value;
                OnPropertyChanged();
            }
        }


        public override DMProperties Clone()
        {
            return new DMProperties
            {
                Availability = Availability,
                IpAddress = IpAddress,
                Port = Port,   
                CorrectionAngle = CorrectionAngle
            };
        }

        public override bool EqualTo(DMProperties model)
        {
            return IpAddress == model.IpAddress
               && Port == model.Port
               && Availability == model.Availability
               && CorrectionAngle == model.CorrectionAngle;
        }

        public override void Update(DMProperties model)
        {
            IpAddress = model.IpAddress;
            Port = model.Port;
            Availability = model.Availability;
            CorrectionAngle = model.CorrectionAngle;
        }
    }
}
