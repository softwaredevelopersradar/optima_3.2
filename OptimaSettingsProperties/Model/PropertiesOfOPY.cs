﻿using OptimaSettingsProperties.Ext;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.WpfPropertyGrid;

namespace OptimaSettingsProperties.Model
{
    [CategoryOrder(nameof(Common), 1)]
    [CategoryOrder(nameof(Map),2)]
    [CategoryOrder(nameof(BRDProperties), 3)]
    [CategoryOrder(nameof(DMProperties), 4)]
    public class PropertiesOfOPY : IModelMethods<PropertiesOfOPY>
    {
       
    public event PropertyChangedEventHandler OnPropertyChanged = (obg, str) => { };

        private void PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(this, e);
        }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(Common))]
        [DisplayName(" ")]
        public Common Common { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(Map))]
        [DisplayName(" ")]
        public MapProperties Map { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(BRDProperties))]
        [DisplayName(" ")]
        public BRDProperties BRD { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(DMProperties))]
        [DisplayName(" ")]
        public DMProperties OEM { get; set; }

        public PropertiesOfOPY()
        {
            BRD = new BRDProperties();
            Map = new MapProperties();
            OEM = new DMProperties();
            Common = new Common();

            BRD.PropertyChanged += PropertyChanged;
            OEM.PropertyChanged += PropertyChanged;
            Map.PropertyChanged += PropertyChanged;
            Common.PropertyChanged += PropertyChanged;
        }
   
        public  bool EqualTo(PropertiesOfOPY model)
        {
            return BRD.EqualTo(model.BRD)
                && Map.EqualTo(model.Map)
                 && OEM.EqualTo(model.OEM)
                 && Common.EqualTo(model.Common);
        }

        public  PropertiesOfOPY Clone()
        {
            return new PropertiesOfOPY
            {
                BRD = BRD.Clone(),
                OEM = OEM.Clone(),
                Common = Common.Clone(),
                Map = Map.Clone()
            };
        }

        public  void Update(PropertiesOfOPY model)
        {
            BRD.Update(model.BRD);
            OEM.Update(model.OEM);
            Common.Update(model.Common);
            Map.Update(model.Map);
        }
    }
}
