﻿using OptimaSettingsProperties.Ext;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
  
namespace OptimaSettingsProperties.Model
{
    public class Common : AbstractBaseProperties<Common>
    {
        private AccessTypes access = AccessTypes.User;
        private Languages language;
        private string fileMap = "";
        private string folderMapTiles = "";
        private ViewCoord coordinateView;
        private string _comPort_Radio = "";
        private string _comPort_GSM = "";
        private bool en = true;
        private bool ru = true;
        private bool az = true;

        public Common() { }

        [NotifyParentProperty(true)]
        public AccessTypes Access
        {
            get => access;
            set
            {
                if (access == value) return;
                access = value;
                Console.WriteLine($"Accsess: {access}");
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public Languages Language
        {
            get => language;
            set
            {
                if (language == value) return;
                language = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public string FileMap
        {
            get => fileMap;
            set
            {
                if (fileMap == value) return;
                fileMap = value;
                OnPropertyChanged();

            }
        }

        [NotifyParentProperty(true)]
        public string FolderMapTiles
        {
            get => folderMapTiles;
            set
            {
                if (folderMapTiles == value) return;
                folderMapTiles = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public ViewCoord CoordinateView
        {
            get => coordinateView;
            set
            {
                if (value == coordinateView) return;             
                coordinateView = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public string ComPort_Radio
        {
            get => _comPort_Radio;
            set
            {
                if (value == _comPort_Radio) return;
                _comPort_Radio = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public string ComPort_GSM
        {
            get => _comPort_GSM;
            set
            {
                if (value == _comPort_GSM) return;
                _comPort_GSM = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public bool IsVisibleEN
        {
            get => en;
            set
            {
                if (en == value) return;
                en = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public bool IsVisibleRU
        {
            get => ru;
            set
            {
                if (ru == value) return;
                ru = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public bool IsVisibleAZ
        {
            get => az;
            set
            {
                if (az == value) return;
                az = value;
                OnPropertyChanged();
            }
        }


        public override Common Clone()
        {
            return new Common
            {
                Access = Access,
                Language = Language,
                FileMap = FileMap,
                FolderMapTiles = FolderMapTiles,
                CoordinateView = CoordinateView,
                ComPort_Radio = ComPort_Radio,
                ComPort_GSM = ComPort_GSM,
                IsVisibleAZ = IsVisibleAZ,
                IsVisibleEN = IsVisibleEN,
                IsVisibleRU = IsVisibleRU
            };
        }

        public override bool EqualTo(Common model)
        {
            return Language == model.Language
                && Access == model.Access
                && FileMap == model.FileMap
                && ComPort_GSM == model.ComPort_GSM
                && ComPort_Radio == model.ComPort_Radio
                && FolderMapTiles == model.FolderMapTiles
                && CoordinateView == model.CoordinateView
                && IsVisibleRU == model.IsVisibleRU
                && IsVisibleEN == model.IsVisibleEN
                && IsVisibleAZ == model.IsVisibleAZ;
        }

        public override void Update(Common model)
        {
            Access = model.Access;
            Language = model.Language;
            FileMap = model.FileMap;
            FolderMapTiles = model.FolderMapTiles;
            CoordinateView = model.CoordinateView;
            ComPort_Radio = model.ComPort_Radio;
            ComPort_GSM = model.ComPort_GSM;
            IsVisibleAZ = model.IsVisibleAZ;
            IsVisibleEN = model.IsVisibleEN;
            IsVisibleRU = model.IsVisibleRU;
        }
    }
}
