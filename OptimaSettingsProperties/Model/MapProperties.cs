﻿using OptimaSettingsProperties.Ext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptimaSettingsProperties.Model
{
    public class MapProperties : AbstractBaseProperties<MapProperties>
    {
        private string imageASY;
        private string imageStation;
        public string ImageASY
        {
            get => imageASY;
            set
            {
                if (imageASY == value) return;
                imageASY = value;
                OnPropertyChanged();
            }
        }

        public string ImageStation
        {
            get => imageStation;
            set
            {
                if (imageStation == value) return;
                imageStation = value;
                OnPropertyChanged();
            }
        }

        public override MapProperties Clone()
        {
            return new MapProperties
            {
                ImageASY = ImageASY,
                ImageStation = ImageStation
            };
        }

        public override bool EqualTo(MapProperties model)
        {
            return ImageStation == model.ImageStation
                && ImageASY == model.ImageASY;
        }

        public override void Update(MapProperties model)
        {
            ImageStation = model.ImageStation;
            ImageASY = model.ImageASY;
        }
    }
}
