﻿using OptimaStationsProperties.Model;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;


namespace OptimaStationsProperties.Editors
{
    public class EditorCustom : PropertyEditor
    {
        Dictionary<Type, string> dictKeyDataTemplate = new Dictionary<Type, string>()
        {
            { typeof(Common), "CommonEditorKey"},
            { typeof(LocationInfo), "LocationInfoEditorKey"},
            { typeof(Connection), "ConnectionEditorKey"}
        };

        public EditorCustom(string PropertyName, Type DeclaringType, Type typeProperty)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/OptimaStationsProperties;component/Editors/Templates.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            this.InlineTemplate = resource[dictKeyDataTemplate[typeProperty]];
        }

        public EditorCustom(string PropertyName, Type DeclaringType, string nameEditorKey)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/OptimaStationsProperties;component/Editors/Templates.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            this.InlineTemplate = resource[nameEditorKey];
        }
    }
}
