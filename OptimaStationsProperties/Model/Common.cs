﻿using OptimaStationsProperties.Ext;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.WpfPropertyGrid;

namespace OptimaStationsProperties.Model
{
    public class Common : AbstractBaseProperties<Common>
    {
        private byte num;
        private String note = " ";
        private Languages language;

        [NotifyParentProperty(true)]
        public byte Num
        {
            get => num;
            set
            {
                if (num == value) return;
                num = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public String Note
        {
            get => note;
            set
            {
                if (note == value) return;
                note = value;
                OnPropertyChanged();
            }
        }

        public Languages Language
        {
            get => language;
            set
            {
                if (language == value) return;
                language = value;
                OnPropertyChanged();
            }
        }

        public override Common Clone()
        {
            return new Common
            {
                Num = Num,
                Note = Note,
                Language = Language
            };
        }

        public override bool EqualTo(Common model)
        {
            return Num == model.Num
                && Note == model.Note
                && Language == model.Language;
        }

        public override void Update(Common model)
        {
            Num = model.Num;
            Note = model.Note;
            Language = model.Language;
        }
    }
}
