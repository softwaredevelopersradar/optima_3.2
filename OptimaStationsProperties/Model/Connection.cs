﻿using OptimaStationsProperties.Ext;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.WpfPropertyGrid;

namespace OptimaStationsProperties.Model
{
    public class Connection : AbstractBaseProperties<Connection>
    {
        private string ipAddress = "127.0.0.1";
        private int port = 88;


        [NotifyParentProperty(true)]
        [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IpAddress
        {
            get => ipAddress;
            set
            {
                if (ipAddress == value) return;
                ipAddress = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Range(1, 1000000)]
        public int Port
        {
            get => port;
            set
            {
                if (port == value) return;
                port = value;
                OnPropertyChanged();
            }
        }

        public override Connection Clone()
        {
            return new Connection
            {
                Port = Port,
                IpAddress = IpAddress
            };
        }

        public override bool EqualTo(Connection model)
        {
            return Port == model.Port
                && IpAddress == model.IpAddress;
        }

        public override void Update(Connection model)
        {
            Port = model.Port;
            IpAddress = model.IpAddress;
        }
    }
}
