﻿using OptimaStationsProperties.Ext;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.WpfPropertyGrid;

namespace OptimaStationsProperties.Model
{
    [CategoryOrder(nameof(Common), 1)]
    [CategoryOrder(nameof(LocationInfo), 2)]
    [CategoryOrder(nameof(Connection), 3)]
    public class PropertiesOfMapObject : IModelMethods<PropertiesOfMapObject>
    {
        public PropertiesOfMapObject()
        {
            Common = new Common();
            LocationInfo = new LocationInfo();
            Connection = new Connection();

            Common.PropertyChanged += PropertyChanged;
            LocationInfo.PropertyChanged += PropertyChanged;
            Connection.PropertyChanged += PropertyChanged;
        }

        public event PropertyChangedEventHandler OnPropertyChanged = (obg, str) => { };

        private void PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(this, e);
        }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(Common))]
        [DisplayName(" ")]
        public Common Common { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(LocationInfo))]
        [DisplayName(" ")]
        public LocationInfo LocationInfo { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(Connection))]
        [DisplayName(" ")]
        public Connection Connection { get; set; }

        public  PropertiesOfMapObject Clone()
        {
            return new PropertiesOfMapObject
            {
                Common = Common.Clone(),
                LocationInfo = LocationInfo.Clone(),
                Connection = Connection.Clone()
            };
        }

        public  bool EqualTo(PropertiesOfMapObject model)
        {
            return Common.EqualTo(model.Common)
                  && LocationInfo.EqualTo(model.LocationInfo)
                  && Connection.EqualTo(model.Connection);
        }

        public  void Update(PropertiesOfMapObject model)
        {
            Common.Update(model.Common);
            LocationInfo.Update(model.LocationInfo);
            Connection.Update(model.Connection);
        }
    }
}
