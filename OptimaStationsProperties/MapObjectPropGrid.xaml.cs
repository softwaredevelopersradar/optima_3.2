﻿using OptimaStationsProperties.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;

namespace OptimaStationsProperties
{
    /// <summary>
    /// Interaction logic for MapObjectPropGrid.xaml
    /// </summary>
    public partial class MapObjectPropGrid : UserControl, INotifyPropertyChanged
    {
        #region NotifyPropertyChanged

            public event PropertyChangedEventHandler PropertyChanged;

            public void OnPropertyChanged([CallerMemberName] string prop = "")
            {
                try
                {
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
                }
                catch { }
            }

            #endregion

        #region Events
            public event EventHandler<PropertiesOfMapObject> OnLocalPropertiesChanged = (sender, newModel) => { };
            public event EventHandler OnLocalDefaultButtonClick = (sender, obj) => { };
            public event EventHandler<PropertiesOfMapObject> OnApplyButtonClick = (sender, obj) => { };
            public event EventHandler<PropertiesOfMapObject> OnDeleteButtonClick = (sender, obj) => { };
        #endregion

        #region Properties

        private bool isLocalSelected;
            public bool IsLocalSelected
            {
                get => isLocalSelected;
                set
                {
                    if (isLocalSelected == value) return;
                    isLocalSelected = value;
                    OnPropertyChanged();
                }
            }

            private bool isValid;
            public bool IsValid
            {
                get => isValid;
                private set
                {
                    if (isValid == value) return;
                    isValid = value;
                    OnPropertyChanged();
                }
            }

            private string errorMessage;
            public string ErrorMessage
            {
                get => errorMessage;
                set
                {
                    if (errorMessage == value) return;
                    errorMessage = value;
                    OnPropertyChanged();
                }
            }

            private bool _showToolTip;
            public bool ShowToolTip
            {
                get => _showToolTip;
                set
                {
                    if (_showToolTip == value) return;
                    _showToolTip = value;
                    OnPropertyChanged();
                }
            }

            private PropertiesOfMapObject savedLocal;

            public PropertiesOfMapObject Local
            {
                get => (PropertiesOfMapObject)Resources["localProperties"];
                set
                {
                    if ((PropertiesOfMapObject)Resources["localProperties"] == value)
                    {
                        savedLocal = value.Clone();
                        return;
                    }
                    ((PropertiesOfMapObject)Resources["localProperties"]).Update(value);
                    savedLocal = value.Clone();
                }
            }
        #endregion

        #region Language
        Dictionary<string, string> TranslateDic;

        private void ChangeLanguage(Languages newLanguage)
        {
            SetDynamicResources(newLanguage);
            LoadDictionary();
            SetCategoryLocalNames();

        }
        private void SetDynamicResources(Languages newLanguage)
        {
            try
            {
                ResourceDictionary dict = new ResourceDictionary();
                switch (newLanguage)
                {
                    case Languages.EN:
                        dict.Source = new Uri("/OptimaStationsProperties;component/Language/StringResource.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case Languages.RU:
                        dict.Source = new Uri("/OptimaStationsProperties;component/Language/StringResource.RU.xaml",
                                           UriKind.Relative);
                        break;
                    case Languages.AZ:
                        dict.Source = new Uri("/OptimaStationsProperties;component/Language/StringResource.AZ.xaml",
                                           UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/OptimaStationsProperties;component/Language/StringResource.EN.xaml",
                                          UriKind.Relative);
                        break;
                }
                Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception)
            {
                //TODO
            }
        }

        void LoadDictionary()
        {
            var translation = Properties.Resources.Translation;
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(translation);
            TranslateDic = new Dictionary<string, string>();
            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlNode x2Node in xRoot.ChildNodes)
            {
                if (x2Node.NodeType == XmlNodeType.Comment)
                    continue;

                // получаем атрибут ID
                if (x2Node.Attributes.Count > 0)
                {
                    XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
                    if (attr != null)
                    {
                        foreach (XmlNode childnode in x2Node.ChildNodes)
                        {
                            // если узел - language
                            if (childnode.Name == Local.Common.Language.ToString())
                            {
                                if (!TranslateDic.ContainsKey(attr.Value))
                                    TranslateDic.Add(attr.Value, childnode.InnerText);
                            }
                        }
                    }
                }
            }
        }

        private void SetCategoryLocalNames()
        {
            try
            {
                foreach (var nameCategory in PropertyLocal.Categories.Select(category => category.Name).ToList())
                {
                    PropertyLocal.Categories.First(t => t.Name == nameCategory).HeaderCategoryName = TranslateDic[nameCategory];
                }
            }
            catch (Exception)
            {

            }
        }
        #endregion

        #region Validation

        private void Validate()
        {
            bool isvalid = true;
            string error = "";
            Action<object> funcValidation = (object obj) =>
            {
                foreach (var property in obj.GetType().GetProperties())
                {
                    foreach (var subProperty in property.PropertyType.GetProperties())
                    {
                        try
                        {
                            if (!(property.Name == "DependencyObjectType" || property.Name == "Dispatcher"))
                            {
                                if (!string.IsNullOrWhiteSpace(Convert.ToString(((IDataErrorInfo)property.GetValue(obj))[subProperty.Name])))
                                {
                                    error += $"{TranslateDic[property.Name]}: {TranslateDic[subProperty.Name]}" + "\n";
                                    isvalid = false;
                                }
                            }
                        }
                        catch
                        {
                            //  Console.WriteLine(property.Name+ "  " + subProperty.Name);
                            continue;
                        }

                    }
                }
                IsValid = isvalid;
                ShowToolTip = !isvalid;
                ErrorMessage = error;
                return;
            };

            funcValidation(Local);

        }

        #endregion
        public MapObjectPropGrid()
        {
                InitializeComponent();
                InitLocalProperties();
                ChangeLanguage(savedLocal.Common.Language);
        }

        private void InitLocalProperties()
        {
            savedLocal = Local.Clone();
            Local.Common.PropertyChanged += Local_PropertyChanged;
            savedLocal.Common.PropertyChanged += SavedLocal_PropertyChanged;
            Local.OnPropertyChanged += OnPropertyChanged;

            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(PropertiesOfMapObject.Common), typeof(PropertiesOfMapObject), typeof(Common)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(PropertiesOfMapObject.Connection), typeof(PropertiesOfMapObject), typeof(Connection)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(PropertiesOfMapObject.LocationInfo), typeof(PropertiesOfMapObject), typeof(LocationInfo)));              
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
           // Validate();
        }

        private void SavedLocal_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(Common.Language):                  
                    ChangeLanguage(savedLocal.Common.Language);
                    break;

                default:
                    break;
            }
        }

        private void Local_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

            switch (e.PropertyName)
            {
                case nameof(Common.Language):
                    ChangeLanguage(Local.Common.Language);
                    break;
                default:
                    break;
            }
        }

        private void ApplyClick(object sender, RoutedEventArgs e)
        {
            OnApplyButtonClick(this, Local);
        }

        private void butDelete_Click(object sender, RoutedEventArgs e)
        {
            OnDeleteButtonClick(this, Local);
        }
    }
}

